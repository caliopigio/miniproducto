#import "Common/Constants.h"
#import "Common/Views/TableImageSubtitleItem.h"
#import "Common/Views/TableImageSubtitleItemCell.h"
#import "Recipes/Constants.h"
#import "Recipes/Models.h"
#import "ItemListDataSource.h"

@implementation ItemListDataSource

#pragma mark -
#pragma mark ItemListDataSoure

- (id)initWithCategoryId:(NSString *)categoryId
{
    self = [super init];
    
    if (self != nil) {
        [self setModel:[[[ItemCollection alloc] initWithCategoryId:categoryId]
                autorelease]];
    }
    return self;
}

- (id)initWithItemId:(NSString *)itemId
{
    self = [super init];
    
    if (self != nil) {
        [self setModel:[[[ItemCollection alloc] initWithItemId:itemId]
                autorelease]];
    }
    return self;
}

#pragma mark -
#pragma mark <TTTableViewDataSource>

- (void)    tableView:(UITableView *)tableView
                 cell:(UITableViewCell *)cell
willAppearAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor colorWithRed:223./255. green:217./255.
            blue:212./255. alpha:1.]];
    
    UILabel *textLabel = [cell textLabel];
    
    [textLabel setBackgroundColor:[UIColor clearColor]];
    [textLabel setShadowColor:[UIColor whiteColor]];
    [textLabel setShadowOffset:CGSizeMake(.0, 1.)];

    UILabel *detailTextLabel = [cell detailTextLabel];
    
    [detailTextLabel setBackgroundColor:[UIColor clearColor]];
    [detailTextLabel setShadowColor:[UIColor whiteColor]];
    [detailTextLabel setShadowOffset:CGSizeMake(.0, 1.)];
    
    if ([indexPath row] == 0) {
        [textLabel setFont:[UIFont boldSystemFontOfSize:20.]];
        [textLabel setTextColor:[UIColor colorWithRed:.0/255. green:99./255.
                blue:54./255. alpha:1.]];
    } else {
        [textLabel setFont:[UIFont boldSystemFontOfSize:16.]];
        [textLabel setTextColor:[UIColor colorWithRed:3./255. green:52./255.
                blue:92./255. alpha:1.]];
        
        [detailTextLabel setFont:[UIFont boldSystemFontOfSize:16.]];
        [detailTextLabel setTextColor:[UIColor colorWithRed:188./255.
                green:106./255. blue:38./255. alpha:1.]];
    }
}

- (void)tableViewDidLoadModel:(UITableView *)tableView
{
    if ([[self items] count] > 0) {
        return;
    }
    NSArray *itemList = [(ItemCollection *)[self model] items];
    
    if ([itemList count] > 0) {
        NSMutableArray *items = [NSMutableArray arrayWithCapacity:
                [itemList count]];
        NSMutableArray *sectionTitles = [NSMutableArray array];

        for (Item *item in itemList) {
            [sectionTitles addObject:@""];

            TTTableImageItem *titleCell = [TTTableImageItem
                    itemWithText:[item name] imageURL:[[item pictureURL]
                        absoluteString]];
            TableImageSubtitleItem *detailCell = [TableImageSubtitleItem
                    itemWithText:[item description]
                        subtitle:[item extraDetails]];
    
            [items addObject:[NSMutableArray arrayWithObjects:
                    titleCell, detailCell, nil]];
        }
        [self setItems:items];
        [self setSections:sectionTitles];
    }
}

- (Class)tableView:(UITableView *)tableView cellClassForObject:(id)object
{
    if ([object isKindOfClass:[TableImageSubtitleItem class]]) {
        return [TableImageSubtitleItemCell class];
    }
    return [super tableView:tableView cellClassForObject:object];
}
@end
