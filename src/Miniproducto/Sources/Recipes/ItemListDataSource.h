#import "Common/Controllers/BaseSectionedDataSource.h"

@interface ItemListDataSource : BaseSectionedDataSource

- (id)initWithCategoryId:(NSString *)categoryId;
- (id)initWithItemId:(NSString *)itemId;
@end
