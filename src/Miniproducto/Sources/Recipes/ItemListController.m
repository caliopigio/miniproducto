#import "Common/Additions/TTStyleSheet+Additions.h"
#import "Recipes/Constants.h"
#import "Recipes/ItemListDataSource.h"
#import "ItemListController.h"

@interface ItemListController (Private)

- (id)initWithTitle:(NSString *)title;
@end

@implementation ItemListController

@synthesize categoryId = _categoryId,
            itemId = _itemId;

#pragma mark -
#pragma mark UITableViewController

- (id)initWithNibName:(NSString *)nibName bundle:(NSBundle *)bundle
{
    self = [super initWithNibName:nibName bundle:bundle];
    
    if (self != nil) {
        [self setTableViewStyle:UITableViewStyleGrouped];
        [[self tableView] setBackgroundColor:[UIColor colorWithPatternImage:
                TTIMAGE(@"bundle://background_texture.png")]];
        
        TTImageView *background = [[TTImageView alloc] initWithFrame:
                [[self view] frame]];
        
        [background setUrlPath:@"bundle://background_texture.png"];
        [[self tableView] setBackgroundView:background];
        [self setVariableHeightRows:YES];
        [[self tableView] setSeparatorColor:[UIColor colorWithRed:103./255.
                green:47./255. blue:.0/255. alpha:1.]];
        [self setStatusBarStyle:UIStatusBarStyleBlackOpaque];
        [self setTitle:NSLocalizedString(kItemListTitle, nil)];
    }
    return self;
}

#pragma mark -
#pragma mark TTTableViewController

- (void)createModel
{
    if (_categoryId != nil) {
        [self setDataSource:[[[ItemListDataSource alloc]
                initWithCategoryId:_categoryId] autorelease]];
    } else if (_itemId != nil) {
        [self setDataSource:[[[ItemListDataSource alloc] initWithItemId:_itemId]
                autorelease]];
    }
}

#pragma mark -
#pragma mark ItemListController

- (id)initWithCategoryId:(NSString *)categoryId title:(NSString *)title
{
    self = [self initWithTitle:title];
    
    if (self != nil) {
        _categoryId = categoryId;
    }
    return self;
}

- (id)initWithItemId:(NSString *)itemId title:(NSString *)title
{
    self = [self initWithTitle:title];
    
    if (self != nil) {
    _itemId = itemId;
    }
    return self;
}

#pragma mark -
#pragma mark ItemListController (Private)

- (id)initWithTitle:(NSString *)title
{
    self = [self initWithNibName:nil bundle:nil];
    
    if (self != nil) {
        [self setTitle:[title stringByReplacingOccurrencesOfString:@"-"
                withString:@" "]];
    }
    return self;
}
@end
