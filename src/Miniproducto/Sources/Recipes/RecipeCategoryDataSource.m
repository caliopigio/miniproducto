#import "Common/Constants.h"
#import "Recipes/Constants.h"
#import "Recipes/Models.h"
#import "Recipes/RecipeCategoryDataSource.h"

@implementation RecipeCategoryDataSource

#pragma mark -
#pragma mark NSObject

- (id)init
{
    self = [super init];
    
    if (self != nil) {
        [self setModel:[[[RecipeCategoryCollection alloc] init] autorelease]];
    }
    return self;
}

#pragma mark -
#pragma mark RecipeCategoryDataSource

- (id)initWithCategoryId:(NSString *)categoryId
{
    self = [super init];
    
    if (self != nil) {
        [self setModel:[[[RecipeCategoryCollection alloc]
                initWithCategoryId:categoryId] autorelease]];
    }
    return self;
}

#pragma mark -
#pragma <TTTableViewController>

- (void)tableViewDidLoadModel:(UITableView *)tableView
{
    if ([[self items] count] > 0) {
        return;
    }
    NSArray *categories = [(RecipeCategoryCollection *)[self model] categories];
    NSUInteger categoryCount = [categories count];

    if (categoryCount > 0) {
        NSMutableArray *items =
                [NSMutableArray arrayWithCapacity:categoryCount];

        for (RecipeCategory *category in categories) {
            BOOL flag = NO;
            TTTableImageItem *item;
            NSString *name = [[category name]
                    stringByReplacingOccurrencesOfString:@" " withString:@"_"];
            if (([[category subcategoriesCount] integerValue] == 0) &&
                    ([[category recipeCount] integerValue] > 0)) {
                item = [TTTableImageItem itemWithText:[category name]
                         imageURL:@"bundle://categories_icon.png"
                            URL:URL(kURLItemListCall, [category categoryId],
                            name)];
                flag = YES;
            } else if ([[category subcategoriesCount] integerValue] > 0) {
                item = [TTTableImageItem itemWithText:[category name]
                        imageURL:@"bundle://categories_icon.png"
                            URL:URL(kURLRecipeSubCategoriesCall,
                            [category categoryId], [category name])];
                flag = YES;
            }
            if (flag) 
                [items addObject:item];
        }
        [self setItems:items];
    }
}
@end
