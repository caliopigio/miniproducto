#import "Common/Controllers/BaseListDataSource.h"

@interface RecipeCategoryDataSource: BaseListDataSource

- (id)initWithCategoryId:(NSString *)categoryId;
@end
