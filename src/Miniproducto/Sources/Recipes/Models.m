#import <Three20/Three20.h>
#import <extThree20JSON/extThree20JSON.h>

#import "Common/Constants.h"
#import "Recipes/Constants.h"
#import "Recipes/Models.h"

// Item collection's key paths
static NSString *const kMutableItemsKey = @"items";
// Recipe category collection's key pathes
static NSString *const kMutableCategoriesKey = @"categories";
// Recipe collection's key pathes
static NSString *const kMutbaleSectionsKey = @"sections";
static NSString *const kMutableSectionTitlesKey = @"sectionTitles";
// Recipe's key pathes
static NSString *const kMutableExtraPictureURLsKey = @"extraPictureURLs";
static NSString *const kMutableIngredientsKey = @"ingredients";
static NSString *const kMutableProceduresKey = @"procedures";
static NSString *const kMutableFeaturesKey = @"features";
static NSString *const kMutableTipsKey = @"tips";
//Meat's key pathes
static NSString *const kMutableMeatsKey = @"meats";
// Recipe misc constants
static NSString *const kRecipeMiscYes = @"YES";

#pragma mark -
#pragma mark Item
#pragma mark -

@implementation Item

@synthesize itemId = _itemId,
            name = _name,
            description = _description,
            extraDetails = _extraDetails,
            pictureURL = _pictureURL;

#pragma mark -
#pragma mark Item

+ (id)itemWithDictionary:(NSDictionary *)rawItem
{
    Item *item = [[[Item alloc] init] autorelease];
    id rawObject = [rawItem objectForKey:kItemIdKey];
    NSNumber *itemId = [rawObject isKindOfClass:[NSNumber class]] ?
            rawObject : nil;
    
    [item setItemId:itemId];
    
    rawObject = [rawItem objectForKey:kItemNameKey];
    NSString *name = [rawObject isKindOfClass:[NSString class]] ?
            rawObject : nil;
    
    [item setName:name];
    
    rawObject = [rawItem objectForKey:kItemDescriptionKey];
    NSString *description = [rawObject isKindOfClass:[NSString class]] ?
            rawObject : nil;
    
    [item setDescription:description];
    
    rawObject = [rawItem objectForKey:kItemExtraDetailsKey];
    NSString *extraDetails = [rawObject isKindOfClass:[NSString class]] ?
            rawObject : nil;
    
    [item setExtraDetails:extraDetails];
    
    rawObject = [rawItem objectForKey:kItemPictureURLKey];
    NSString *pictureURL = [rawObject isKindOfClass:[NSString class]] ?
            rawObject : nil;
    
    [item setPictureURL:[NSURL URLWithString:pictureURL]];
    return item;
}

- (id)initWithItemId:(NSString *)itemId
{
    self = [super init];
    
    if (self != nil) {
        _itemId = [NSNumber numberWithInteger:[itemId integerValue]];
    }
    return self;
}

- (void)copyPropertiesFromItem:(Item *)item
{
    [self setItemId:[item itemId]];
    [self setName:[item name]];
    [self setDescription:[item description]];
    [self setExtraDetails:[item extraDetails]];
}

#pragma mark -
#pragma mark <TTModel>

- (void)load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more
{
    if (![self isLoading]) {
        TTURLRequest *request = [TTURLRequest requestWithURL:
                URL(kURLItemDetailEndpoint, _itemId) delegate:self];
        
        ADD_DEFAULT_CACHE_POLICY_TO_REQUEST(request, cachePolicy);
        [request setResponse:[[[TTURLJSONResponse alloc] init] autorelease]];
        [request send];
    }
}

#pragma mark <TTURLRequestDelegate>

- (void)requestDidFinishLoad:(TTURLRequest *)request
{
    NSDictionary *rootObject = [(TTURLJSONResponse *)[request response]
            rootObject];
    Item *item = [Item itemWithDictionary:rootObject];
    
    [self copyPropertiesFromItem:item];
    [super requestDidFinishLoad:request];
}

@end

#pragma mark -
#pragma mark ItemCollection
#pragma mark

@implementation ItemCollection

@synthesize categoryId = _categoryId,
            itemId = _itemId,
            items = _items;

#pragma mark -
#pragma mark NSObject

- (id)init
{
    self = [super init];
    
    if (self != nil) {
        _items = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [_items release];
    [super dealloc];
}

#pragma mark -
#pragma mark NSObject (NSKeyValueCoding)

- (void)insertObject:(Item *)object inItemsAtIndex:(NSUInteger)index
{
    [_items insertObject:object atIndex:index];
}

- (void)insertItems:(NSArray *)array atIndexes:(NSIndexSet *)indexes
{
    [_items insertObjects:array atIndexes:indexes];
}

- (void)removeObjectFromItemsAtIndex:(NSUInteger)index
{
    [_items removeObjectAtIndex:index];
}

- (void)removeItemsAtIndexes:(NSIndexSet *)indexes
{
    [_items removeObjectsAtIndexes:indexes];
}

#pragma mark -
#pragma mark ItemCollection

- (id)initWithCategoryId:(NSString *)categoryId
{
    self = [self init];
    
    if (self != nil) {
        _categoryId = categoryId;
    }
    return self;
}

- (id)initWithItemId:(NSString *)itemId
{
    self = [self init];
    
    if (self != nil) {
        _itemId = itemId;
    }
    return self;
}

#pragma mark -
#pragma mark <TTModel>

- (void)load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more
{
    if (![self isLoading]) {
        NSString *url;
        
        if (_categoryId != nil) {
            url = URL(kURLItemCollectionEndpoint, _categoryId);
        } else if (_itemId != nil) {
            url = URL(kURLItemDetailEndpoint, _itemId);
        } else {
            return;
        }
        TTURLRequest *request = [TTURLRequest requestWithURL:url
                 delegate:self];
        
        ADD_DEFAULT_CACHE_POLICY_TO_REQUEST(request, cachePolicy);
        [request setResponse:[[[TTURLJSONResponse alloc] init] autorelease]];
        [request send];
    }
}

#pragma mark -
#pragma mark <TTURLRequestDelegate>

- (void)requestDidFinishLoad:(TTURLRequest *)request
{
    NSDictionary *rootObject = [(TTURLJSONResponse *)[request response]
            rootObject];
    id rawObject = [rootObject objectForKey:kItemCollectionItemsKey];
    NSArray *rawItems = [rawObject isKindOfClass:[NSArray class]] ?
            rawObject : nil;
    NSMutableArray *mutableItems = [self mutableArrayValueForKey:
            kMutableItemsKey];
    
    for (NSDictionary *rawItem in rawItems) {
        Item *item = [Item itemWithDictionary:rawItem];
        
        [mutableItems addObject:item];
    }
    [super requestDidFinishLoad:request];
}
@end

#pragma mark -
#pragma mark RecipeCategory
#pragma mark -

@implementation RecipeCategory

#pragma mark -
#pragma mark RecipeCategory

@synthesize categoryId = _categoryId,
            name = _name,
            recipeCount = _recipeCount,
            subcategoriesCount = _subcategoriesCount,
            details = _details;

+ (id)shortRecipeCategoryFromDictionary:(NSDictionary *)rawRecipeCategory
{
    RecipeCategory *category = [[[RecipeCategory alloc] init] autorelease];
    id rawObject = [rawRecipeCategory objectForKey:kRecipeCategoryIdKey];
    NSNumber *categoryId = [rawObject isKindOfClass:[NSNumber class]] ?
            rawObject : nil;
    
    [category setCategoryId:categoryId];
    
    rawObject = [rawRecipeCategory objectForKey:kRecipeCategoryNameKey];
    NSString *name = [rawObject isKindOfClass:[NSString class]] ?
            rawObject : nil;
    
    [category setName:name];
    return category;
}

+ (id)recipeCategoryFromDictionary:(NSDictionary *)rawRecipeCategory
{
    RecipeCategory *category =
            [self shortRecipeCategoryFromDictionary:rawRecipeCategory];
    id rawObject = [rawRecipeCategory objectForKey:kRecipeCategoryCountKey];
    NSNumber *recipeCount = [rawObject isKindOfClass:[NSNumber class]] ?
            rawObject : nil;
    
    [category setRecipeCount:recipeCount];

    rawObject = [rawRecipeCategory objectForKey:kRecipeSubcategoriesCountKey];
    NSNumber *subcategoriesCount = [rawObject isKindOfClass:[NSNumber class]] ?
            rawObject : nil;
    
    [category setSubcategoriesCount:subcategoriesCount];

    rawObject = [rawRecipeCategory objectForKey:kRecipeCategoryDetailsKey];
    NSNumber *details = [rawObject isKindOfClass:[NSNumber class]] ?
            rawObject : nil;

    [category setDetails:details];
    return category;
}
@end

#pragma mark -
#pragma mark RecipeCategoryCollection
#pragma mark -

@implementation RecipeCategoryCollection

@synthesize categoryId = _categoryId,
            details = _details,
            categories = _categories;

#pragma mark -
#pragma mark NSObject

- (id)init
{
    self = [super init];
    
    if (self != nil) {
        _categories = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [_categories release];
    [super dealloc];
}

#pragma mark -
#pragma mark NSObject (NSKeyValueCoding)

- (void)insertObject:(NSURL *)categories inCategoriesAtIndex:(NSUInteger)index
{
    [_categories insertObject:categories atIndex:index];
}

- (void)insertCategories:(NSArray *)categories atIndexes:(NSIndexSet *)indexes
{
    [_categories insertObjects:categories atIndexes:indexes];
}

- (void)removeObjectFromCategoriesAtIndex:(NSUInteger)index
{
    [_categories removeObjectAtIndex:index];
}

- (void)removeCategoriesAtIndexes:(NSIndexSet *)indexes
{
    [_categories removeObjectsAtIndexes:indexes];
}

#pragma mark -
#pragma mark RecipeCategoryCollection

- (id)initWithCategoryId:(NSString *)categoryId
{
    if ((self = [self init]) != nil) {
        _categoryId = categoryId;
    }
    return self;
}

#pragma mark -
#pragma mark <TTModel>

- (void)load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more
{
    if (![self isLoading]) {
        NSString *url;
        
        if (_categoryId == nil) {
            url = kURLRecipeCategoriesEndpoint;
        } else {
            url = URL(kURLRecipeSubcategoryEndpoint, _categoryId);
        }
        TTURLRequest *request = [TTURLRequest requestWithURL:url delegate:self];
        
        ADD_DEFAULT_CACHE_POLICY_TO_REQUEST(request, cachePolicy);
        [request setResponse:[[[TTURLJSONResponse alloc] init] autorelease]];
        [request send];
    }
}

#pragma mark -
#pragma mark <TTURLRequestDelegate>

- (void)requestDidFinishLoad:(TTURLRequest *)request
{
    NSDictionary *rootObject = [(TTURLJSONResponse *)[request response]
            rootObject];
    id rawObject = [rootObject objectForKey:
            kRecipeCategoryCollectionCategoriesKey];
    NSArray *rawCategories = [rawObject isKindOfClass:[NSArray class]] ?
            rawObject : nil;
    NSMutableArray *mutableCategories = [self mutableArrayValueForKey:
            kMutableCategoriesKey];

    for (NSDictionary *rawCategory in rawCategories) {
        RecipeCategory *recipeCategory =
                [RecipeCategory recipeCategoryFromDictionary:rawCategory];

        [mutableCategories addObject:recipeCategory];
    }
    [super requestDidFinishLoad:request];
}
@end
