#import "Common/Additions/TTStyleSheet+Additions.h"
#import "Recipes/Constants.h"
#import "Recipes/RecipeCategoryDataSource.h"
#import "Recipes/RecipeCategoryController.h"

@implementation RecipeCategoryController

#pragma mark -
#pragma mark UITableViewController

- (id)initWithNibName:(NSString *)nibName bundle:(NSBundle *)bundle
{
    self = [super initWithNibName:nibName bundle:bundle];
    
    if (self != nil) {
        [self setTitle:kRecipeCategoryTitle];
    }
    return self;
}

#pragma mark -
#pragma mark TTTableViewController

- (void)createModel
{
    if (_categoryId == nil) {
        [self setDataSource:
                [[[RecipeCategoryDataSource alloc] init] autorelease]];
    }
    else {
        [self setDataSource:[[[RecipeCategoryDataSource alloc]
                initWithCategoryId:_categoryId] autorelease]];
    }
}

#pragma mark -
#pragma mark RecipeCategoryController

@synthesize categoryId = _categoryId;

- (id)initWithCategoryId:(NSString *)categoryId title:(NSString *)title
{
    self = [self initWithNibName:nil bundle:nil];
    
    if (self != nil) {
        _categoryId = categoryId;
        
        [self setTitle:title];
    }
    return self;
}

- (id)initWithCategoryId:(NSString *)categoryId
{
    self = [self initWithCategoryId:categoryId title:
            NSLocalizedString(kRecipeSubcategoryTitle, nil)];
    
    return self;
}
@end
