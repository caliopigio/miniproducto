#import "Common/Controllers/Three20/ReconnectableTableViewController.h"

@interface ItemListController : ReconnectableTableViewController
{
    NSString *_categoryId;
    NSString *_itemId;
}
@property (nonatomic, retain) NSString *categoryId;
@property (nonatomic, retain) NSString *itemId;

- (id)initWithCategoryId:(NSString *)categoryId title:(NSString *)title;
- (id)initWithItemId:(NSString *)itemId title:(NSString *)title;
@end
