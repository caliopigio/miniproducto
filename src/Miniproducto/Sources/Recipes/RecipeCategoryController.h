#import "Common/Controllers/BaseTableViewController.h"

@interface RecipeCategoryController: BaseTableViewController
{
    NSString *_categoryId;
    UIView *_headerView;
    UILabel *_titleLabel;
}
@property (nonatomic, copy) NSString *categoryId;

- (id)initWithCategoryId:(NSString *)categoryId title:(NSString *)title;
- (id)initWithCategoryId:(NSString *)categoryId;
@end
