#import "Common/Models/URLRequestModel.h"

#pragma mark -
#pragma mark Item
#pragma mark -

@interface Item : URLRequestModel
{
    NSNumber *_itemId;
    NSString *_name;
    NSString *_description;
    NSString *_extraDetails;
    NSURL *_pictureURL;
}
@property (nonatomic, retain) NSNumber *itemId;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, retain) NSString *extraDetails;
@property (nonatomic, retain) NSURL *pictureURL;

+ (id)itemWithDictionary:(NSDictionary *)rawItem;
- (id)initWithItemId:(NSString *)itemId;
- (void)copyPropertiesFromItem:(Item *)item;
@end

#pragma mark -
#pragma mark ItemCollection
#pragma mark -

@interface ItemCollection : URLRequestModel
{
    NSString *_categoryId;
    NSString *_itemId;
    NSMutableArray *_items;
}
@property (nonatomic, retain) NSString *categoryId;
@property (nonatomic, retain) NSString *itemId;
@property (nonatomic, readonly) NSArray *items;

- (id)initWithCategoryId:(NSString *)categoryId;
- (id)initWithItemId:(NSString *)itemId;
@end

#pragma mark -
#pragma mark RecipeCategory
#pragma mark -

@interface RecipeCategory: NSObject
{
    NSNumber *_categoryId;
    NSString *_name;
    NSNumber *_recipeCount;
    NSNumber *_subcategoriesCount;
    NSNumber *_details;
}
@property (nonatomic, retain) NSNumber *categoryId;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSNumber *recipeCount;
@property (nonatomic, retain) NSNumber *subcategoriesCount;
@property (nonatomic, retain) NSNumber *details;

+ (id)shortRecipeCategoryFromDictionary:(NSDictionary *)rawRecipeCategory;
+ (id)recipeCategoryFromDictionary:(NSDictionary *)rawRecipeCategory;
@end

#pragma mark -
#pragma mark RecipeCategoryCollection
#pragma mark -

@interface RecipeCategoryCollection: URLRequestModel
{
    NSString *_categoryId;
    NSNumber *_details;
    NSMutableArray *_categories;
}
@property (nonatomic, readonly) NSArray *categories;
@property (nonatomic, retain) NSNumber *details;
@property (nonatomic, retain) NSString *categoryId;

- (id)initWithCategoryId:(NSString *)categoryId;
@end
