#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import <Three20/Three20.h>

#import "Application/Constants.h"
#import "Application/StyleSheet.h"

// status bar style
#define STATUS_BAR_STYLE UIStatusBarStyleBlackOpaque

// navigation and tool bars color
#define BAR_TINT_COLOR RGBCOLOR(203., 154., 115.)
#define TOOLBAR_TINT_COLOR RGBCOLOR(154., 117., 54.)

// text in tables color
#define TABLE_TEXT_COLOR RGBCOLOR(.0, .0, .0)
#define CAPTION_ALT_COLOR RGBCOLOR(227., 13., 23.)
#define DETAIL_ALT_COLOR RGBCOLOR(.0, .0, .0)
#define TABLE_HEADER_SHADOW_COLOR RGBCOLOR(73., 34., 9.)

// text in tables sizes
#define TABLE_TEXT_HEADER_SIZE 16.
#define TABLE_SECTION_HEADER_HEIGHT 24.
#define PICTURE_HEADER_SIZE 14.
#define TABLE_TEXT_SIZE 13.

// text in headers color
#define HEADER_COLOR_YELLOW [UIColor whiteColor]
#define HEADER_COLOR_WHITE [UIColor whiteColor]
#define HEADER_SHADOW_COLOR RGBCOLOR(57., 45., 27.)

// launcher
#define LAUNCHER_BACKGROUND TTIMAGE(@"bundle://launcher-background.png")
#define NAVIGATION_BAR_LOGO TTIMAGE(@"bundle://navBar_logo.png")
#define NAVIGATION_BAR_COLOR RGBCOLOR(255., 208., .0)
#define LAUNCHER_FONT_COLOR RGBCOLOR(57., 45., 27.)
#define LAUNCHER_FONT_SHADOW RGBCOLOR(255., 255., 255.)

// stores
#define STORES_BACKGROUND TTIMAGE(@"bundle://stores-background.png")
#define STORES_SECTION_HEADER TTIMAGE(@"bundle://stores-section-header.png")

// recipes
#define RECIPES_BACKGROUND TTIMAGE(@"bundle://recipes-background.png")
#define MEATS_BACKGROUND TTIMAGE(@"bundle://meats-background.png")
#define RECIPES_SECTION_HEADER TTIMAGE(@"bundle://recipes-section-header.png")
#define MEATS_SECTION_HEADER TTIMAGE(@"bundle://meats-section-header.png")
#define RECIPE_DETAIL_BACKGROUND \
        TTIMAGE(@"bundle://recipes-picture-background.png")

@implementation StyleSheet

#pragma mark -
#pragma mark General styles

- (UIStatusBarStyle)statusBarStyle
{
    return STATUS_BAR_STYLE;
}

- (UIColor *)navigationBarTintColor
{
    return BAR_TINT_COLOR;
}

- (UIColor *)toolbarTintColor
{
    return TOOLBAR_TINT_COLOR;
}

- (UIColor *)captionAltColor
{
    return CAPTION_ALT_COLOR;
}

- (UIColor *)headerColorYellow
{
    return HEADER_COLOR_YELLOW;
}

- (UIColor *)headerColorWhite
{
    return HEADER_COLOR_WHITE;
}

- (UIColor *)headerShadowColor
{
    return HEADER_SHADOW_COLOR;
}

- (UIFont *)tableTextHeaderFont
{
    return [UIFont boldSystemFontOfSize:TABLE_TEXT_HEADER_SIZE];
}

- (CGFloat)heightForTableSectionHeaderView
{
    return TABLE_SECTION_HEADER_HEIGHT;
}

- (UIColor *)tableHeaderTintColor
{
    return [UIColor yellowColor];
}

- (UIColor *)tableHeaderShadowColor
{
    return HEADER_COLOR_WHITE;
}

- (CGSize)tableHeaderShadowOffset
{
    return CGSizeMake(.0, 1.);
}

- (UIFont *)tableTextFont
{
    return [UIFont boldSystemFontOfSize:TABLE_TEXT_SIZE];
}

- (UIFont *)pictureHeaderFont
{
    return [UIFont boldSystemFontOfSize:PICTURE_HEADER_SIZE];
}

- (UIFont *)tableSummaryFont
{
    return [UIFont boldSystemFontOfSize:TABLE_TEXT_HEADER_SIZE];
}

- (UIColor *)tableHeaderTextColor
{
    return [UIColor colorWithRed:.0/255. green:99./255. blue:54./255. alpha:1.];
}

#pragma mark -
#pragma mark Launcher

- (TTStyle *)launcherButton:(UIControlState)state
{
    return [TTPartStyle styleWithName:@"image" 
            style:TTSTYLESTATE(launcherButtonImage:, state) next:
                [TTTextStyle styleWithFont:[UIFont boldSystemFontOfSize:12]
                color:LAUNCHER_FONT_COLOR minimumFontSize:8
                shadowColor:LAUNCHER_FONT_SHADOW
                shadowOffset:CGSizeMake(.0, -1.) next:nil]];
}

- (UIImageView *)launcherBackgroundImage
{
    return [[[UIImageView alloc] 
            initWithImage:LAUNCHER_BACKGROUND] autorelease];
}

- (UIImage *)navigationBarLogo
{
    return NAVIGATION_BAR_LOGO;
}

#pragma mark -
#pragma mark Stores

- (UIImage *)storesBackgroundHeader
{
    return STORES_BACKGROUND;
}

- (TTStyle *)storesSectionHeader
{
    return [TTImageStyle styleWithImage:STORES_SECTION_HEADER next:nil];
}

#pragma mark -
#pragma mark Recipes

- (UIImage *)recipesBackgroundHeader
{
    return RECIPES_BACKGROUND;
}

- (TTStyle *)recipesSectionHeader
{
    return  [TTImageStyle styleWithImage:RECIPES_SECTION_HEADER next:nil];
}

- (UIImage *)meatsBackgroundHeader
{
    return MEATS_BACKGROUND;
}

- (TTStyle *)meatsSectionHeader
{
    return  [TTImageStyle styleWithImage:MEATS_SECTION_HEADER next:nil];
}

- (UIImage *)recipePictureBackground
{
    return RECIPE_DETAIL_BACKGROUND;
}
@end
