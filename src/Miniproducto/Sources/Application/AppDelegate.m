#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

#import <Three20/Three20.h>
#import <extThree20JSON/extThree20JSON.h>

#import "Common/Constants.h"
#import "Launcher/Constants.h"
#import "Launcher/LauncherViewController.h"
#import "Launcher/ListLauncherController.h"

#import "Recipes/Constants.h"
//#import "Recipes/MeatListController.h"
#import "Recipes/RecipeCategoryController.h"
//#import "Recipes/RecipeListController.h"
//#import "Recipes/RecipeDetailController.h"
//#import "Recipes/IngredientRecipeDetailController.h"
//#import "Recipes/ProcedureRecipeDetailController.h"
//#import "Recipes/TipsRecipeDetailController.h"
//#import "Recipes/ContributionRecipeDetailController.h"
//#import "Recipes/FeaturesRecipeDetailController.h"
#import "Recipes/ItemListController.h"

#import "Stores/Constants.h"
#import "Stores/RegionListController.h"
#import "Stores/StoreListController.h"
#import "Stores/StoreDetailController.h"
//#import "Stores/StoreMapController.h"

#import "Contact/Constants.h"
#import "Contact/ContactDetailController.h"

#import "Application/StyleSheet.h"
#import "Application/Constants.h"
#import "Application/AppDelegate.h"

@interface AppDelegate ()

@property (nonatomic, retain) UIWindow *overlay;

- (void)fadeDefault;
- (BOOL)loadDefaults;
@end

@implementation AppDelegate

#pragma mark -
#pragma mark NSObject

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_window release];
    [_facebook release];
    [_twitter release];
    [_context release];
    [_model release];
    [_coordinator release];
    [_dateFormatter release];
    [super dealloc];
}

#pragma mark -
#pragma mark AppDelegate (Public)

@synthesize window = _window, facebook = _facebook, twitter = _twitter,
        overlay = _overlay, receivedData = _receivedData;

- (NSString *)getUUID {
    //get a UUID value from UserDefaults
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *uuidStr = [defaults stringForKey:kApplicationUUIDKey];

    if (uuidStr == nil) {
        CFUUIDRef uuidObject = CFUUIDCreate(kCFAllocatorDefault);

        uuidStr = [(NSString *)CFUUIDCreateString(
                kCFAllocatorDefault, uuidObject) autorelease];
        CFRelease(uuidObject);
        [defaults setObject:uuidStr forKey:kApplicationUUIDKey];
        [defaults synchronize];
    }
    return uuidStr;
}

#pragma mark -
#pragma mark AppDelegate (Private)

- (void)fadeDefault
{
    UIImageView *splash = [[[UIImageView alloc] initWithImage:
            [UIImage imageNamed:@"Default.png"]] autorelease];
    
    [splash setTag:100];
    [_window insertSubview:splash atIndex:0];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.8];
    [splash setAlpha:.0];
    [UIView commitAnimations];
    [splash removeFromSuperview];
}

- (BOOL)loadDefaults
{
    [self getUUID];
    BOOL registrationIsComplete = YES;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults objectForKey:kDNIDefault] == nil)
        registrationIsComplete = NO;
    if ([defaults objectForKey:kPhoneDefault] == nil)
        registrationIsComplete = NO;
    if ([defaults objectForKey:kEmailDefault] == nil)
        registrationIsComplete = NO;
    return registrationIsComplete;
}

#pragma mark -
#pragma mark <UIApplicationDelegate>

- (BOOL)            application:(UIApplication *)application
  didFinishLaunchingWithOptions:(NSDictionary *)options
{
    // Conf Facebook
    _facebook = [[Facebook alloc] initWithAppId:kAppId andDelegate:self];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults objectForKey:kAccessTokenKey] &&
            [defaults objectForKey:kExpirationDateKey]) {
        [_facebook setAccessToken:[defaults objectForKey:kAccessTokenKey]];
        [_facebook setExpirationDate:
                [defaults objectForKey:kExpirationDateKey]];
    }
    // Conf Twitter
    _twitter = [[SA_OAuthTwitterEngine alloc] initOAuthWithDelegate:self];
    
    [_twitter setConsumerKey:kOAuthConsumerKey];
    [_twitter setConsumerSecret:kOAuthConsumerSecret];
    
    // Set the maxContentLength to auto
    [[TTURLRequestQueue mainQueue] setMaxContentLength:0];
    
    // Style sheet
    [TTStyleSheet setGlobalStyleSheet:[[[StyleSheet alloc] init]
          autorelease]];
   
    UIImage *navBarBackground = [UIImage imageNamed:@"navBar"];
    
    [[UINavigationBar appearance] setBackgroundImage:navBarBackground
            forBarMetrics:UIBarMetricsDefault];
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
            [UIColor colorWithRed:103./255. green:47./255. blue:.0/255.
                alpha:1.], UITextAttributeTextColor,
            [UIColor colorWithRed:1. green:1. blue:1.
                alpha:.5], UITextAttributeTextShadowColor,
            //[UIColor colorWithRed:203./255. green:154./255. blue:115./255.
            //    alpha:1.], UITextAttributeTextShadowColor,
            [NSValue valueWithUIOffset:UIOffsetMake(.0, 1.)],
                UITextAttributeTextShadowOffset,
                nil];
    
    [[UINavigationBar appearance] setTitleTextAttributes:textAttributes];
    
    /*textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
            //[UIColor colorWithRed:188./255. green:122./255. blue:68./255.
            //    alpha:1.], UITextAttributeTextColor,
            [UIColor colorWithRed:103./255. green:47./255. blue:.0/255.
                alpha:1.], UITextAttributeTextShadowColor,
            //[NSValue valueWithUIOffset:UIOffsetMake(.0, 1.)],
            //    UITextAttributeTextShadowOffset,
                nil];*/
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:textAttributes
            forState:UIControlStateNormal];
    [[UIBarButtonItem appearance] setTitleTextAttributes:textAttributes
            forState:UIControlStateHighlighted];

    // View and window
    _window = [[UIWindow alloc] initWithFrame:TTScreenBounds()];

    TTNavigator *navigator = [TTNavigator navigator];
    [navigator setWindow:_window];
    TTURLMap *map = [navigator URLMap];

    // Launcher
    [map from:kURLLauncher
            toViewController:[ListLauncherController class]];
    
    // Contact
    [map from:kURLContactDetailController
            toViewController:[ContactDetailController class]];

    // Recipes
    [map from:kURLItemList
            toViewController:[ItemListController class]];
    [map from:kURLItemDetail
            toViewController:[ItemListController class]];
    /*[map from:kURLMeats
            toModalViewController:[MeatListController class]
                transition:UIModalTransitionStyleFlipHorizontal];*/
    [map from:kURLRecipeCategories
            toViewController:[RecipeCategoryController class]];
    [map from:kURLRecipeSubcategories
            toViewController:[RecipeCategoryController class]];
    /*[map from:kURLRecipeList
            toViewController:[RecipeListController class]];
    [map from:kURLRecipeMeatList
            toViewController:[RecipeListController class]];
    [map from:kURLRecipeDetail
            toViewController:[RecipeDetailController class]];
    [map from:kURLRecipeMeatsDetail
            toViewController:[RecipeDetailController class]];
    [map from:kURLIngredientRecipeDetail
            toViewController:[IngredientRecipeDetailController class]];
    [map from:kURLProceduresRecipeDetail
            toViewController:[ProcedureRecipeDetailController class]];
    [map from:kURLTipsRecipeDetail
            toViewController:[TipsRecipeDetailController class]];
    [map from:kURLIngredientRecipeMeatsDetail
            toViewController:[IngredientRecipeDetailController class]];
    [map from:kURLProceduresRecipeMeatsDetail
            toViewController:[ProcedureRecipeDetailController class]];
    [map from:kURLTipsRecipeMeatsDetail
            toViewController:[TipsRecipeDetailController class]];
    [map from:kURLContributionRecipeDetail
            toViewController:[ContributionRecipeDetailController class]];
    [map from:kURLFeaturesRecipeDetail
            toViewController:[FeaturesRecipeDetailController class]];*/
    // Stores
    [map from:kURLRegionList
            toViewController:[RegionListController class]];
    [map from:kURLSubregionList
            toViewController:[RegionListController class]];
    [map from:kURLStoreList
            toViewController:[StoreListController class]];
    [map from:kURLStoreDetail
            toViewController:[StoreDetailController class]];
    /*[map from:kURLStoreMap
            toModalViewController:[StoreMapController class]
                transition:UIModalTransitionStyleFlipHorizontal];
    [map from:kURLStoreDetailMap
            toModalViewController:[StoreMapController class]
                transition:UIModalTransitionStyleFlipHorizontal];*/
    // Open root view controller
    [self fadeDefault];
    [navigator openURLAction:
            [[TTURLAction actionWithURLPath:kURLLauncherCall]
                applyAnimated:YES]];
    /*if (![self loadDefaults]) {
        TSAlertView *alert = [[TSAlertView alloc]
                initWithTitle:kRegistrationTitle message:nil delegate:self
                    cancelButtonTitle:nil otherButtonTitles:kRegsitrationButton,
                    nil];
        
        [alert setStyle:TSAlertViewStyleInput];
        [[alert firstTextField] setPlaceholder:kDNILabel];
        [alert addTextFieldWithLabel:kPhoneLabel value:nil];
        [alert addTextFieldWithLabel:kEmailLabel value:nil];
        [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
        [[alert textFieldAtIndex:1] setKeyboardType:UIKeyboardTypeNumberPad];
        [[alert textFieldAtIndex:2] setKeyboardType:UIKeyboardTypeEmailAddress];
        [[alert textFieldAtIndex:2] setAutocapitalizationType:
                UITextAutocapitalizationTypeNone];
        [alert show];
    }*/
    [_window makeKeyAndVisible];
    return YES;
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)URL
{
    /*[[TTNavigator navigator] openURLAction:
            [[TTURLAction actionWithURLPath:
                [URL absoluteString]] applyAnimated:YES]];
    return YES;*/
    return [_facebook handleOpenURL:URL];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [_facebook handleOpenURL:url];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the
    // application terminates.
    [self saveContext];
}

#pragma mark -
#pragma mark <TTNavigatorDelegate>

- (BOOL)navigator:(TTNavigator *)navigator shouldOpenURL:(NSURL *)URL
{
    // FIXME: Opens URLs systematically!!!
    return YES;
}

#pragma mark -
#pragma mark <FBSessionDelegate>

- (void)fbDidLogin
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:[_facebook accessToken] forKey:kAccessTokenKey];
    [defaults setObject:[_facebook expirationDate] forKey:kExpirationDateKey];
    [defaults synchronize];
}

- (void)fbSessionInvalidated
{
    
}

- (void)fbDidLogout
{
    
}

- (void)fbDidExtendToken:(NSString *)accessToken expiresAt:(NSDate *)expiresAt
{
    
}

- (void)fbDidNotLogin:(BOOL)cancelled
{
    
}

#pragma mark -
#pragma mark <SA_OAuthTwitterEngineDelegate>

- (void)storeCachedTwitterOAuthData:(NSString *)data
                        forUsername:(NSString *)username
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:data forKey:kOAuthData];
    [defaults synchronize];
}

- (NSString *)cachedTwitterOAuthDataForUsername:(NSString *)username
{    
    return [[NSUserDefaults standardUserDefaults] objectForKey:kOAuthData];
}

#pragma mark -
#pragma mark <TSAlertViewDelegate>

- (void)   alertView:(TSAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != [alertView cancelButtonIndex]) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        if ([[[alertView textFieldAtIndex:0] text] length] == 8) {
            [defaults setObject:[[alertView textFieldAtIndex:0] text]
                    forKey:kDNIDefault];
        } else {
            [[alertView textFieldAtIndex:0] setText:nil];
        }
        if (([[[alertView textFieldAtIndex:1] text] length] == 9) ||
                ([[[alertView textFieldAtIndex:1] text] length] == 7)) {
            [defaults setObject:[[alertView textFieldAtIndex:1] text]
                    forKey:kPhoneDefault];
        } else {
            [[alertView textFieldAtIndex:1] setText:nil];
        }   
        if ([[[alertView textFieldAtIndex:2] text] length] > 0) {
            [defaults setObject:[[alertView textFieldAtIndex:2] text]
                    forKey:kEmailDefault];
        } else {
            [[alertView textFieldAtIndex:2] setText:nil];
        }
        if ([self loadDefaults]) {
            NSURL *url = [NSURL URLWithString:ENDPOINT(@"/register/")];
            NSMutableURLRequest *request = [NSMutableURLRequest
                    requestWithURL:url
                        cachePolicy:NSURLRequestUseProtocolCachePolicy
                        timeoutInterval:60.];
            
            [request setHTTPMethod:kPostHTTPMethod];
            [request setValue:kContentHTTPHeaderValue
                    forHTTPHeaderField:kContentHTTPHeaderKey];
            
            NSString *postString = [NSString stringWithFormat:
                    kRegisterRequestString,
                    kDNIResquestKey, [defaults objectForKey:kDNIDefault],
                    kPhoneRequestKey, [defaults objectForKey:kPhoneDefault],
                    kEmailRequestKey, [defaults objectForKey:kEmailDefault]];
            
            [request setHTTPBody:[postString dataUsingEncoding:
                    NSUTF8StringEncoding]];
                        
            NSURLConnection *connection = [[[NSURLConnection alloc]
                    initWithRequest:request delegate:self] autorelease];
            
            if (connection) {
                _receivedData = [[NSMutableData data] retain];
            }
        }
    }
}

#pragma mark -
#pragma mark <NSURLConnectionDelegate>

- (void)connection:(NSURLConnection *)connection
didReceiveResponse:(NSURLResponse *)response
{
    [_receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[[NSString alloc] initWithData:_receivedData
                encoding:NSUTF8StringEncoding] autorelease];
    
    NSLog(@"%@", response);
    [_receivedData release];
}
@end
