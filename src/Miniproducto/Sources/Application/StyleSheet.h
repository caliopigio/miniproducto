#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import <Three20/Three20.h>

@interface StyleSheet: TTDefaultStyleSheet

// General
- (UIStatusBarStyle)statusBarStyle;
- (UIColor *)navigationBarTintColor;
- (UIColor *)toolbarTintColor;
- (UIColor *)captionAltColor;
- (UIColor*)headerColorYellow;
- (UIColor *)headerColorWhite;
- (UIColor *)headerShadowColor;
- (UIFont *)tableTextHeaderFont;
- (CGFloat)heightForTableSectionHeaderView;
- (UIFont *)tableTextFont;
- (UIFont *)pictureHeaderFont;
- (UIFont *)tableSummaryFont;
- (UIColor *)tableHeaderTextColor;

// Launcher
- (TTStyle *)launcherButton:(UIControlState)state;
- (UIImageView *)launcherBackgroundImage;
- (UIImage *)navigationBarLogo;

// Stores
- (UIImage *)storesBackgroundHeader;
- (TTStyle *)storesSectionHeader;

// Recipes
- (UIImage *)recipesBackgroundHeader;
- (TTStyle *)recipesSectionHeader;
- (UIImage *)meatsBackgroundHeader;
- (TTStyle *)meatsSectionHeader;
- (UIImage *)recipePictureBackground;
@end
