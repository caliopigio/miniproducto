#import "Common/Controllers/BaseListDataSource.h"

@interface RegionListDataSource: BaseListDataSource

- (id)initWithRegionId:(NSString *)regionId;
@end