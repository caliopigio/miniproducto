#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

#import "Common/Models/URLRequestModel.h"

#pragma mark -
#pragma mark Subregion
#pragma mark -

@interface Subregion: NSObject
{
    NSNumber *_subregionId;
    NSString *_name;
}
@property (nonatomic, retain) NSNumber *subregionId;
@property (nonatomic, retain) NSString *name;

+ (id)subregionFromDictionary:(NSDictionary *)rawSubregion;
@end

#pragma mark -
#pragma mark SubregionCollection
#pragma mark -

@interface SubregionCollection: URLRequestModel
{
    NSNumber *_regionId;
    NSMutableArray *_subregions;
}
@property (nonatomic, retain) NSNumber *regionId;
@property (nonatomic, readonly) NSArray *subregions;

+ (id)subregionCollectionFromDictionary:(NSDictionary *)rawCollection;

- (id)initWithRegionId:(NSString *)regionId;
- (void)copyPropertiesFromSubregionCollection:(SubregionCollection *)collection;
@end

#pragma mark -
#pragma mark Region
#pragma mark -

@interface Region: NSObject
{
    NSNumber *_regionId;
    NSString *_name;
    NSNumber *_count;
    NSNumber *_suggested;
}
@property (nonatomic, retain) NSNumber *regionId;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSNumber *count;
@property (nonatomic, retain) NSNumber *suggested;

+ (id)shortRegionFromDictionary:(NSDictionary *)rawRegion;
+ (id)regionFromDictionary:(NSDictionary *)rawRegion;
@end

#pragma mark -
#pragma mark RegionCollection
#pragma mark -

@interface RegionCollection: URLRequestModel
{
    NSMutableArray *_regions;
}
@property (nonatomic, readonly) NSArray *regions;

+ (id)regionCollectionFromDictionary:(NSDictionary *)rawCollection;

- (void)copyPropertiesFromRegionCollection:(RegionCollection *)collection;
@end

#pragma mark -
#pragma mark MapAnnotation
#pragma mark -

@interface MapAnnotation: NSObject <MKAnnotation>
{
    CLLocationCoordinate2D _coordinate;
    NSString *_title;
    NSString *_subtitle;
    NSString *_pictureURL;
    NSNumber *_storeId;
}
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, copy) NSString *pictureURL;
@property (nonatomic, retain) NSNumber *storeId;

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate;
@end

#pragma mark -
#pragma mark Store
#pragma mark -

@interface Store: URLRequestModel
{
    NSNumber *_storeId;
    NSString *_name;
    NSString *_storeAddress;
    NSURL *_pictureURL;
    NSString *_code;
    NSString *_attendance;
    Region *_region;
    Subregion *_subregion;
    Subregion *_district;
    NSString *_ubigeo;
    NSString *_phones;
    NSNumber *_latitude;
    NSNumber *_longitude;
}
@property (nonatomic, retain) NSNumber *storeId;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *storeAddress;
@property (nonatomic, retain) NSURL *pictureURL;
@property (nonatomic, retain) NSString *code;
@property (nonatomic, retain) NSString *attendance;
@property (nonatomic, retain) NSNumber *latitude;
@property (nonatomic, retain) NSNumber *longitude;
@property (nonatomic, retain) Region *region;
@property (nonatomic, retain) Subregion *subregion;
@property (nonatomic, retain) Subregion *district;
@property (nonatomic, retain) NSString *ubigeo;
@property (nonatomic, retain) NSString *phones;

+ (id)shortStoreFromDictionary:(NSDictionary *)rawStore 
       whithLatitudeInLocation:(BOOL)latitudeInLocation;
+ (id)storeFromDictionary:(NSDictionary *)rawStore;

- (id)initWithStoreId:(NSString *)storeId;
- (void)copyPropertiesFromStore:(Store *)store;
@end

#pragma mark -
#pragma mark StoreCollection
#pragma mark -

@interface StoreCollection: URLRequestModel
{
    NSNumber *_regionId;
    NSNumber *_subregionId;
    NSMutableArray *_stores;
    NSMutableArray *_districts;
}
@property (nonatomic, retain) NSNumber *regionId;
@property (nonatomic, retain) NSNumber *subregionId;
@property (nonatomic, readonly) NSArray *stores;
@property (nonatomic, readonly) NSArray *districts;

+ (id)storeCollectionFromDictionary:(NSDictionary *)rawCollection;

- (id)initWithSubregionId:(NSString *)subregionId
              andRegionId:(NSString *)regionId;
- (void)copyPropertiesFromStoreCollection:(StoreCollection *)collection;
@end
