#import "Common/Controllers/BaseTableViewController.h"
#import "Stores/StoreListDataSource.h"

@interface StoreListController : BaseTableViewController
        <StoreListDataSourceDelegate, MKMapViewDelegate>
{
    NSString *_subregionId;
    NSString *_regionId;
    MKMapView *_mapView;
    CGFloat _mapHeight;
    MKCoordinateRegion _region;
    UITapGestureRecognizer *_gesture;
    UIButton *_userLocationButton;
}
@property (nonatomic, copy) NSString *subregionId;
@property (nonatomic, copy) NSString *regionId;

- (id)initWithSubregionId:(NSString *)subregionId
              andRegionId:(NSString *)regionId
                     name:(NSString *)name;
@end
