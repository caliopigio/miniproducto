#import "Common/Additions/TTStyleSheet+Additions.h"
#import "Stores/Constants.h"
#import "Stores/RegionListDataSource.h"
#import "Stores/SubregionDataSource.h"
#import "Stores/RegionListController.h"

@implementation RegionListController

#pragma mark -
#pragma mark UITableViewController

- (id)initWithNibName:(NSString *)nibName bundle:(NSBundle *)bundle
{
    self = [super initWithNibName:nibName bundle:bundle];
    
    if (self != nil) {
        [self setTitle:kRegionListTitle];

    }
    return self;
}

#pragma mark -
#pragma mark TTTableViewController

- (void)createModel
{
    if (_regionId != nil) {
        [self setDataSource:[[[SubregionDataSource alloc] initWithRegionId:
                _regionId] autorelease]];
    } else {
        [self setDataSource:[[[RegionListDataSource alloc] init] autorelease]];
    }
}

#pragma mark -
#pragma mark RegionListController

@synthesize regionId = _regionId;

- (id)initWithRegionId:(NSString *)regionId name:(NSString *)name
{
    self = [self initWithNibName:nil bundle:nil];
    
    if (self != nil) {
        _regionId = regionId;
        
        [self setTitle:name];
    }
    return self;
}
@end
