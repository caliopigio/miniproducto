#import "Common/Controllers/BaseTableViewController.h"
#import "Stores/StoreDetailDataSource.h"

@interface StoreDetailController : ReconnectableTableViewController
        <StoreDetailDataSourceDelegate, MKMapViewDelegate>
{
    NSString *_storeId;
    MKMapView *_mapView;
    CGFloat _mapHeight;
    MKCoordinateRegion _region;
    UITapGestureRecognizer *_gesture;
    UIButton *_userLocationButton;
}
@property (nonatomic, retain) NSString *storeId;

- (id)initWithStoreId:(NSString *)storeId;
@end

