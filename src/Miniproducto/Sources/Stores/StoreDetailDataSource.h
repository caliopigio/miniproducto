#import <MapKit/MapKit.h>

#import "Common/Controllers/BaseListDataSource.h"

@protocol StoreDetailDataSourceDelegate;

@interface StoreDetailDataSource: BaseListDataSource
{
    id<StoreDetailDataSourceDelegate> _delegate;
}
@property (nonatomic, assign) id delegate;

- (id)initWithStoreId:(NSString *)storeId
             delegate:(id<StoreDetailDataSourceDelegate>)delegate;
@end

@protocol StoreDetailDataSourceDelegate <NSObject>

@optional
- (void)dataSource:(StoreDetailDataSource *)dataSource
  mapViewForHeader:(MKMapView *)view;
- (void)dataSource:(StoreDetailDataSource *)dataSource
         storeName:(NSString *)name;
@end

