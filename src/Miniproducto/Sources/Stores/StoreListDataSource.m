#import "Common/Constants.h"
#import "Common/Views/TableImageSubtitleItem.h"
#import "Common/Views/TableImageSubtitleItemCell.h"
#import "Stores/Constants.h"
#import "Stores/Models.h"
#import "Stores/StoreListDataSource.h"

CGFloat const kHeaderHieght = 150.;

@interface StoreListDataSource (Private)

- (MKMapView *)mapViewWithStores:(NSArray *)stores;
@end

@implementation StoreListDataSource

#pragma mark -
#pragma mark NSObject

- (void)dealloc
{
    [super dealloc];
}

#pragma mark -
#pragma mark StoreListDataSource (public)

@synthesize delegate = _delegate;


- (id)initWithSubregionId:(NSString *)subregionId
              andRegionId:(NSString *)regionId
{
    self = [self initWithSubregionId:subregionId regionId:regionId
            delegate:nil];
    
    return self;
}

- (id)initWithSubregionId:(NSString *)subregionId
                 regionId:(NSString *)regionId
                 delegate:(id<StoreListDataSourceDelegate>)delegate
{
    self = [super init];
    
    if (self != nil) {
        [self setModel:[[[StoreCollection alloc]initWithSubregionId:subregionId
                andRegionId:regionId] autorelease]];
        
        _delegate = delegate;
    }
    return self;
}

#pragma mark -
#pragma mark StoreListDataSource (Private)

- (MKMapView *)mapViewWithStores:(NSArray *)stores
{
    CGRect frame = [[UIScreen mainScreen] bounds];
    frame.size.height = kHeaderHieght;
    MKMapView *_mapView = [[MKMapView alloc] initWithFrame:frame];
    
    [_mapView setDelegate:(id<MKMapViewDelegate>)_delegate];
    for (NSArray *sections in stores) {
        for (Store *store in sections) {
            CLLocationCoordinate2D coordinate;
            coordinate.latitude = [[store latitude] doubleValue];
            coordinate.longitude = [[store longitude] doubleValue];
            MapAnnotation *annotation = [[[MapAnnotation alloc]
                    initWithCoordinate:coordinate] autorelease];
            
            [annotation setTitle:[store name]];
            [annotation setSubtitle:[store storeAddress]];
            [annotation setPictureURL:[[store pictureURL] absoluteString]];
            [annotation setStoreId:[store storeId]];
            [_mapView addAnnotation:annotation];
        }
    }
    float minLatitude = .0, minLongitude = .0, maxLatitude = .0,
            maxLongitude = .0;
    
    for (MapAnnotation *annotation in [_mapView annotations]) {
        minLatitude = minLatitude == .0 ? [annotation coordinate].latitude :
                MIN(minLatitude, [annotation coordinate].latitude);
        minLongitude = minLongitude == .0 ? [annotation coordinate].longitude :
                MIN(minLongitude, [annotation coordinate].longitude);
        maxLatitude = maxLatitude == .0 ? [annotation coordinate].latitude :
                MAX(maxLatitude, [annotation coordinate].latitude);
        maxLongitude = maxLongitude == .0 ? [annotation coordinate].longitude :
                MAX(maxLongitude, [annotation coordinate].longitude);
    }
    CLLocation *pointA = [[[CLLocation alloc] initWithLatitude:minLatitude
            longitude:minLongitude] autorelease];
    CLLocation *pointB = [[[CLLocation alloc] initWithLatitude:maxLatitude
            longitude:minLongitude] autorelease];
    MKCoordinateSpan span = MKCoordinateSpanMake((maxLatitude - minLatitude),
            (maxLongitude - minLongitude));
    CLLocationCoordinate2D center;
    center.latitude = maxLatitude - (span.latitudeDelta / 2);
    center.longitude = maxLongitude - (span.longitudeDelta / 2);
    CLLocationDistance distance = [pointA distanceFromLocation:pointB];
    
    [_mapView setMapType:MKMapTypeStandard];
    [_mapView setZoomEnabled:NO];
    [_mapView setScrollEnabled:NO];
    if (distance <= minRegion) {
        [_mapView setRegion:MKCoordinateRegionMakeWithDistance
         (center, minRegion, minRegion)];
    } else {
        [_mapView setRegion:MKCoordinateRegionMake(center, span)];
    }
    return _mapView;
}

- (void)tableViewDidLoadModel:(UITableView *)tableView
{
    if ([[self items] count] > 0) {
        return;
    }
    StoreCollection *collection = (StoreCollection *)[self model];
    NSArray *sections = [collection stores];
    
    if ([_delegate respondsToSelector:@selector(dataSource:mapViewForHeader:)])
    {
        [_delegate dataSource:self mapViewForHeader:
                [self mapViewWithStores:sections]];
    }
    if ([sections count] > 0) {
        NSMutableArray *items =
                [NSMutableArray arrayWithCapacity:[sections count]];
        
        for (NSArray *section in sections) {
            NSMutableArray *sectionItems =
                    [NSMutableArray arrayWithCapacity:[sections count]];
            
            for (Store *store in section) {
                TableImageSubtitleItem *item = [TableImageSubtitleItem
                        itemWithText:[store storeAddress] subtitle:nil
                            URL:URL(kURLStoreDetailCall, [store storeId])];
                
                [sectionItems addObject:item];
            }
            [items addObject:sectionItems];
        }
        [self setSections:[[collection districts] mutableCopy]];
        [self setItems:items];
    }
}

- (Class)tableView:(UITableView *)tableView cellClassForObject:(id)object
{
    if ([object isKindOfClass:[TableImageSubtitleItem class]])
        return [TableImageSubtitleItemCell class];
    return [super tableView:tableView cellClassForObject:object];
}
@end