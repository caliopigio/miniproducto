#import "Common/Controllers/BaseSectionedDataSource.h"
#import "Stores/Models.h"

@protocol StoreListDataSourceDelegate;

@interface StoreListDataSource: BaseSectionedDataSource
{
    id<StoreListDataSourceDelegate> _delegate;
}
@property (nonatomic, assign) id<StoreListDataSourceDelegate> delegate;

- (id)initWithSubregionId:(NSString *)subregionId
              andRegionId:(NSString *)regionId;
- (id)initWithSubregionId:(NSString *)subregionId
                 regionId:(NSString *)regionId
                 delegate:(id<StoreListDataSourceDelegate>)delegate;
@end

@protocol StoreListDataSourceDelegate <NSObject>

@optional
- (void)dataSource:(StoreListDataSource *)dataSource
  mapViewForHeader:(MKMapView *)view;
@end