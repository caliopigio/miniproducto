#import "Common/Constants.h"
#import "Common/Additions/TTStyleSheet+Additions.h"
#import "Stores/Models.h"
#import "Stores/Constants.h"
#import "Stores/StoreDetailDataSource.h"

static CGFloat kHeaderHeight = 220.;

@interface StoreDetailDataSource ()

- (MKMapView *)mapViewWithStore:(Store *)store;
@end

@implementation StoreDetailDataSource

#pragma mark -
#pragma mark NSObject

- (void) dealloc
{
    [self setDelegate:nil];
    [super dealloc];
}

#pragma mark -
#pragma mark StoreDetailDataSource

@synthesize delegate = _delegate;

- (id)initWithStoreId:(NSString *)storeId
             delegate:(id<StoreDetailDataSourceDelegate>)delegate
{
    self = [super init];
    
    if (self != nil) {
        [self setModel:[[[Store alloc] initWithStoreId:storeId] autorelease]];

        _delegate = delegate;
    }
    return self;
}

#pragma mark -
#pragma mark StoreDetailDataSource (private)

- (MKMapView *)mapViewWithStore:(Store *)store
{
    CGRect frame = [[UIScreen mainScreen] bounds];
    frame.size.height = kHeaderHeight;
    MKMapView *_mapView = [[MKMapView alloc] initWithFrame:frame];
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = [[store latitude] doubleValue];
    coordinate.longitude = [[store longitude] doubleValue];
    MapAnnotation *annotation = [[[MapAnnotation alloc] initWithCoordinate:
            coordinate] autorelease];
    
    [_mapView setDelegate:(id<MKMapViewDelegate>)_delegate];
    [annotation setTitle:[store name]];
    [annotation setSubtitle:[store storeAddress]];
    [annotation setPictureURL:[[store pictureURL] absoluteString]];
    [_mapView addAnnotation:annotation];
    [_mapView setMapType:MKMapTypeStandard];
    [_mapView setZoomEnabled:NO];
    [_mapView setScrollEnabled:NO];
    [_mapView setRegion:MKCoordinateRegionMakeWithDistance
            (coordinate, minRegion, minRegion)];
    return _mapView;
}

#pragma mark -
#pragma mark <TTtableViewDelegate>

- (void)    tableView:(UITableView *)tableView
                 cell:(UITableViewCell *)cell
willAppearAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor colorWithRed:223./255. green:217./255.
            blue:212./255. alpha:1.]];
    
    UILabel *textLabel = [cell textLabel];
    
    [textLabel setBackgroundColor:[UIColor clearColor]];
    [textLabel setFont:[UIFont boldSystemFontOfSize:16.]];
    [textLabel setTextColor:[UIColor colorWithRed:143./255. green:65./255.
            blue:0./255. alpha:1.]];
    [textLabel setShadowColor:[UIColor whiteColor]];
    [textLabel setShadowOffset:CGSizeMake(.0, 1.)];
    
    textLabel = [cell detailTextLabel];
    
    [textLabel setBackgroundColor:[UIColor clearColor]];
    [textLabel setFont:[UIFont boldSystemFontOfSize:16.]];
    [textLabel setTextColor:[UIColor colorWithRed:0./255. green:99./255.
            blue:54./255. alpha:1.]];
    [textLabel setShadowColor:[UIColor whiteColor]];
    [textLabel setShadowOffset:CGSizeMake(.0, 1.)];
}


- (void)tableViewDidLoadModel:(UITableView *)tableView
{
    if ([[self items] count] > 0) {
        return;
    }
    Store *store = (Store *)[self model];
    TTTableCaptionItem *address = [TTTableCaptionItem
            itemWithText:[store storeAddress] caption:kStoreDetailAddress];
    TTTableCaptionItem *attendance = [TTTableCaptionItem
            itemWithText:[store attendance] caption:kStoreDetailAttendance];
    TTTableCaptionItem *phones = [TTTableCaptionItem
            itemWithText:[store phones] caption:kStoreDetailPhones];
    NSMutableArray *items = [NSArray arrayWithObjects:address, attendance,
            phones, nil];
    
    if ([_delegate respondsToSelector:@selector(dataSource:mapViewForHeader:)])
    {
        [_delegate dataSource:self mapViewForHeader:
                [self mapViewWithStore:store]];
    }
    if ([_delegate respondsToSelector:@selector(dataSource:storeName:)]) {
        [_delegate dataSource:self storeName:[store name]];
    }
    [self setItems:items];
}
@end
