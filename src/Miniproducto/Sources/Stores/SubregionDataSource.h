#import "Common/Controllers/BaseListDataSource.h"

@interface SubregionDataSource: BaseListDataSource
{
    NSString *_regionId;
}
@property (nonatomic, retain) NSString *regionId;

- (id)initWithRegionId:(NSString *)regionId;
@end