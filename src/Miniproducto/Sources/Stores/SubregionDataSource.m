#import "Common/Constants.h"
#import "Stores/Models.h"
#import "Stores/Constants.h"
#import "Stores/SubregionDataSource.h"

@implementation SubregionDataSource

@synthesize regionId = _regionId;

#pragma mark -
#pragma mark RegionListDataSource

- (id)initWithRegionId:(NSString *)regionId
{
    self = [super init];
    
    if (self != nil) {
        _regionId = [regionId copy];
        
        [self setModel:[[[SubregionCollection alloc] initWithRegionId:regionId]
                autorelease]];
    }
    return self;
}

#pragma mark -
#pragma mark <TTTableViewController>

- (void)tableViewDidLoadModel:(UITableView *)tableView
{
    if ([[self items] count] > 0) {
        return;
    }
    SubregionCollection *regionCollection = (SubregionCollection *)[self model];
    NSArray *subregions = [regionCollection subregions];
    NSMutableArray *items = [NSMutableArray array];
    
    for (Subregion *subregion in subregions) {
        NSString *name = [subregion name];
        TTTableImageItem *item = [TTTableImageItem itemWithText:name imageURL:
                @"bundle://stores_icon.png" URL:URL(kURLStoreListCall,
                    [subregion subregionId], _regionId, [subregion name])];
        
        [items addObject:item];
    }
    [self setItems:items];
}
@end
