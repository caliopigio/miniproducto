#import "Common/Additions/TTStyleSheet+Additions.h"
#import "Common/Constants.h"
#import "Stores/Constants.h"
#import "Stores/Models.h"
#import "Stores/StoresTableViewDelegate.h"
#import "Stores/StoreDetailController.h"

static CGFloat kMargin = 10.;

@interface StoreDetailController()

- (void)buttonTapped:(id)sender;
- (void)showBigMap;
- (void)locateUser:(id)sender;
@end

@implementation StoreDetailController

@synthesize storeId = _storeId;

#pragma mark -
#pragma mark TTTableViewController

- (void)createModel
{
    [self setDataSource:[[[StoreDetailDataSource alloc]
            initWithStoreId:_storeId delegate:self] autorelease]];
}

#pragma mark -
#pragma mark StoreDetailController

- (id)initWithStoreId:(NSString *)storeId
{
    self = [self initWithNibName:nil bundle:nil];
    
    if (self != nil) {
        _storeId = storeId;

        [self setTableViewStyle:UITableViewStyleGrouped];
        [[self tableView] setBackgroundColor:[UIColor colorWithPatternImage:
                TTIMAGE(@"bundle://background_texture.png")]];
        
        TTImageView *background = [[TTImageView alloc] initWithFrame:
                [[self view] frame]];
        
        [background setUrlPath:@"bundle://background_texture.png"];
        [[self tableView] setBackgroundView:background];
        [self setVariableHeightRows:YES];
        [[self tableView] setSeparatorColor:[UIColor colorWithRed:103./255.
                green:47./255. blue:.0/255. alpha:1.]];
    }
    return self;
}

#pragma mark -
#pragma mark StoreListController (Private)

- (void)buttonTapped:(id)sender
{
    [_userLocationButton removeFromSuperview];
    CGRect frame = [_mapView frame];
    frame.size.height = _mapHeight;
    
    [UIView animateWithDuration:.5 animations:^{
        [_mapView setFrame:frame];
    }];
    [_mapView setScrollEnabled:NO];
    [_mapView setZoomEnabled:NO];
    [_mapView setRegion:_region animated:YES];
    for (MapAnnotation *annotation in [_mapView annotations]) {
        [_mapView deselectAnnotation:annotation animated:YES];
    }
    [_mapView addGestureRecognizer:_gesture];
    [_tableView setTableHeaderView:_mapView];
    [_tableView setScrollEnabled:YES];
    [[self navigationItem] setRightBarButtonItem:nil animated:YES];
}

- (void)showBigMap
{
    CGRect frame = [[_tableView tableHeaderView] frame];
    UIView *blank = [[UIView alloc] initWithFrame:frame];
    
    [_tableView setTableHeaderView:blank];
    
    CGRect viewFrame = [[self view] frame];
    frame = [_mapView frame];
    frame.size.height = viewFrame.size.height;
    
    [_mapView setScrollEnabled:YES];
    [_mapView setZoomEnabled:YES];
    [_mapView removeGestureRecognizer:_gesture];
    [[self view] addSubview:_mapView];
    [UIView animateWithDuration:.5 animations:^{
        [_mapView setFrame:frame];
    }];

    UIBarButtonItem *button = [[[UIBarButtonItem alloc]
            initWithTitle:@"Mostrar detalles" style:UIBarButtonItemStyleBordered
                target:self action:@selector(buttonTapped:)] autorelease];
    
    [[self navigationItem] setRightBarButtonItem:button animated:YES];
    
    frame = CGRectMake(viewFrame.size.width - (50. + kMargin),
            viewFrame.size.height - (36. + kMargin), 50., 36.);
    _userLocationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [_userLocationButton setFrame:frame];
    [_userLocationButton setBackgroundImage:[UIImage
            imageNamed:@"translucent_button"] forState:UIControlStateNormal];
    [_userLocationButton setImage:[UIImage imageNamed:@"needle"]
            forState:UIControlStateNormal];
    [_userLocationButton addTarget:self action:@selector(locateUser:)
            forControlEvents:UIControlEventTouchUpInside];
    [[self view] performSelector:@selector(addSubview:)
            withObject:_userLocationButton afterDelay:.5];
}

- (void)locateUser:(id)sender
{
    [_mapView setShowsUserLocation:YES];
}

#pragma mark -
#pragma mark <MKMapViewDelegate>

- (void)        mapView:(MKMapView *)mapView
  didUpdateUserLocation:(MKUserLocation *)userLocation
{
    CLLocationCoordinate2D currentLocation =
            [[userLocation location] coordinate];
    
    for (MapAnnotation *annotation in [_mapView annotations]) {
        if ([[annotation title] isEqualToString:kStoreMapCurrentLocation])
            [_mapView removeAnnotation:annotation];
    }
    MapAnnotation *location = [[[MapAnnotation alloc]
            initWithCoordinate:currentLocation] autorelease];
    
    [location setTitle:kStoreMapCurrentLocation];
    [_mapView addAnnotation:location];
    
    float minLatitude = .0, minLongitude = .0, maxLatitude = .0,
    maxLongitude = .0;
    
    for (MapAnnotation *annotation in [_mapView annotations]) {
        minLatitude = minLatitude == .0 ? [annotation coordinate].latitude :
                MIN(minLatitude, [annotation coordinate].latitude);
        minLongitude = minLongitude == .0 ? [annotation coordinate].longitude :
                MIN(minLongitude, [annotation coordinate].longitude);
        maxLatitude = maxLatitude == .0 ? [annotation coordinate].latitude :
                MAX(maxLatitude, [annotation coordinate].latitude);
        maxLongitude = maxLongitude == .0 ? [annotation coordinate].longitude :
                MAX(maxLongitude, [annotation coordinate].longitude);
    }
    CLLocation *pointA = [[[CLLocation alloc] initWithLatitude:minLatitude
            longitude:minLongitude] autorelease];
    CLLocation *pointB = [[[CLLocation alloc] initWithLatitude:maxLatitude
            longitude:minLongitude] autorelease];
    MKCoordinateSpan span = MKCoordinateSpanMake((maxLatitude - minLatitude),
            (maxLongitude - minLongitude));
    CLLocationCoordinate2D center;
    center.latitude = maxLatitude - (span.latitudeDelta / 2);
    center.longitude = maxLongitude - (span.longitudeDelta / 2);
    CLLocationDistance distance = [pointA distanceFromLocation:pointB];
    
    if (distance <= minRegion) {
        [_mapView setRegion:MKCoordinateRegionMakeWithDistance
         (center, minRegion, minRegion) animated:YES];
    } else {
        [_mapView setRegion:MKCoordinateRegionMake(center, span) animated:YES];
    }
    [_mapView selectAnnotation:location animated:YES];
    if ([[userLocation location] horizontalAccuracy] <= 100.)
        [_mapView setShowsUserLocation:NO];
    [_mapView performSelector:@selector(setShowsUserLocation:) withObject:NO
            afterDelay:10];
}

- (void)                mapView:(MKMapView *)mapView
   didFailToLocateUserWithError:(NSError *)error
{
    [_mapView setShowsUserLocation:NO];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView
            viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([[annotation title] isEqualToString:kStoreMapCurrentLocation]) {
        MKPinAnnotationView *pin = (MKPinAnnotationView *)[_mapView
                dequeueReusableAnnotationViewWithIdentifier:kPinAnnotationId];
        
        if (pin == nil) {
            pin = [[[MKPinAnnotationView alloc] initWithAnnotation:annotation
                    reuseIdentifier:kPinAnnotationId] autorelease];
        }
        [pin setPinColor:MKPinAnnotationColorGreen];
        [pin setCanShowCallout:YES];
        [pin setAnimatesDrop:YES];
        return pin;
    }
    NSString *pinId = @"pinId";
    MKAnnotationView *view = [mapView
            dequeueReusableAnnotationViewWithIdentifier:pinId];
    
    if (view == nil) {
        view = [[[MKAnnotationView alloc] initWithAnnotation:annotation
                reuseIdentifier:pinId] autorelease];
    }
    [view setImage:[UIImage imageNamed:kStoreMapAnnotationImage]];
    [view setCanShowCallout:YES];
    return view;
}

#pragma mark -
#pragma mark <StoreDetailDataSourceDelegate>

- (void)dataSource:(StoreDetailDataSource *)dataSource
  mapViewForHeader:(MKMapView *)view
{
    _mapView = view;
    _mapHeight = [_mapView frame].size.height;
    _region = [_mapView region];
    _gesture = [[UITapGestureRecognizer alloc] initWithTarget:self
            action:@selector(showBigMap)];
    
    [_mapView addGestureRecognizer:_gesture];
    [[self tableView] setTableHeaderView:_mapView];
}

- (void)dataSource:(StoreDetailDataSource *)dataSource
         storeName:(NSString *)name
{
    [self setTitle:name];
}
@end
