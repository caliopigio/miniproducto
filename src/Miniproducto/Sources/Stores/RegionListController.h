#import "Common/Controllers/BaseTableViewController.h"

@interface RegionListController: BaseTableViewController
{
    NSString *_regionId;
}
@property (nonatomic, copy) NSString *regionId;

- (id)initWithRegionId:(NSString *)regionId name:(NSString *)name;
@end