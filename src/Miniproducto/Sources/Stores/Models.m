#import <Three20/Three20.h>
#import <extThree20JSON/extThree20JSON.h>

#import "Common/Constants.h"
#import "Stores/Constants.h"
#import "Stores/Models.h"

// RegionCollection's key path
static NSString *const kMutableRegionsKey = @"regions";

// SubregionCollection's key path
static NSString *const kMutableSubregionsKey = @"subregions";

// StoreCollection's key path
static NSString *const kMutableStoresKey = @"stores";
static NSString *const kMutableStoreTitlesKey = @"districts";

// Store's key pathes
static NSString *const kMutableServicesKey = @"services";

#pragma mark -
#pragma mark Subregion
#pragma mark -

@implementation Subregion

@synthesize subregionId = _subregionId, name = _name;

#pragma mark -
#pragma mark Subregion

+ (id)subregionFromDictionary:(NSDictionary *)rawSubregion
{
    Subregion *subregion = [[[Subregion alloc] init] autorelease];
    id rawObject = [rawSubregion objectForKey:kRegionIdKey];
    NSNumber *subregionId = [rawObject isKindOfClass:[NSNumber class]] ?
                rawObject : nil;
    
    [subregion setSubregionId:subregionId];

    rawObject = [rawSubregion objectForKey:kRegionNameKey];
    NSString *name = [rawObject isKindOfClass:[NSString class]] ?
                rawObject : nil;

    [subregion setName:name];
    return subregion;
}
@end

#pragma mark -
#pragma mark SubregionCollection
#pragma mark -

@implementation SubregionCollection

@synthesize subregions = _subregions,
            regionId = _regionId;

#pragma mark -
#pragma mark NSObject

- (id)init
{
    self = [super init];
    
    if (self != nil) {
        _subregions = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [_subregions release];
    [super dealloc];
}

#pragma mark -
#pragma mark NSObject (NSKeyValueCoding)

- (void)    insertObject:(Subregion *)subregion
     inSubregionsAtIndex:(NSUInteger)index
{
    [_subregions insertObject:subregion atIndex:index];
}

- (void)insertSubregions:(NSArray *)subregions atIndexes:(NSIndexSet *)indexes
{
    [_subregions insertObjects:subregions atIndexes:indexes];
}

- (void)removeObjectFromSubregionsAtIndex:(NSUInteger)index
{
    [_subregions removeObjectAtIndex:index];
}

- (void)removeSubregionsAtIndexes:(NSIndexSet *)indexes
{
    [_subregions removeObjectsAtIndexes:indexes];
}

#pragma mark -
#pragma mark SubregionCollection

+ (id)subregionCollectionFromDictionary:(NSDictionary *)rawCollection
{
    SubregionCollection *collection = [[[SubregionCollection alloc] init]
            autorelease];
    NSMutableArray *mutableSubregions = [collection mutableArrayValueForKey:
            kMutableSubregionsKey];
    id rawObject = [rawCollection objectForKey:kSubregionCollectionRegionsKey];
    NSArray *subregions = [rawObject isKindOfClass:[NSArray class]] ?
            rawObject : nil;

    for (NSDictionary *rawSubregion in subregions) {
        Subregion *subregion = [Subregion subregionFromDictionary:rawSubregion];
       
        [mutableSubregions addObject:subregion];
    }
    return collection;
}

- (id)initWithRegionId:(NSString *)regionId
{
    self = [self init];
    
    if (self != nil) {
        _regionId = [NSNumber numberWithInteger:[regionId integerValue]];
    }
    return self;
}

- (void)copyPropertiesFromSubregionCollection:(SubregionCollection *)collection
{
    NSMutableArray *mutableSubregions = [self mutableArrayValueForKey:
            kMutableSubregionsKey];
    
    [mutableSubregions addObjectsFromArray:[collection subregions]];
}

#pragma mark -
#pragma mark <TTModel>

- (void)load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more
{
    if (![self isLoading]) {
        TTURLRequest *request = [TTURLRequest requestWithURL:
                URL(kSubregionListEndPoint, _regionId) delegate:self];
        
        ADD_DEFAULT_CACHE_POLICY_TO_REQUEST(request, cachePolicy);
        [request setResponse:[[[TTURLJSONResponse alloc] init] autorelease]];
        [request send];
    }
}

#pragma mark -
#pragma mark <TTURLRequestDelegate>

- (void)requestDidFinishLoad:(TTURLRequest *)request
{
    NSDictionary *rootObject = [(TTURLJSONResponse *)[request response]
            rootObject];
    SubregionCollection *collection = [SubregionCollection
            subregionCollectionFromDictionary:rootObject];
    
    if (collection == nil) {
        [self didFailLoadWithError:BACKEND_ERROR([request urlPath], rootObject)
                tryAgain:NO];
        return;
    }
    [self copyPropertiesFromSubregionCollection:collection];
    [super requestDidFinishLoad:request];
}
@end

#pragma mark -
#pragma mark Region
#pragma mark -

@implementation Region

@synthesize regionId = _regionId,
            name = _name,
            count = _count,
            suggested = _suggested;

#pragma mark -
#pragma mark Region

+ (id)shortRegionFromDictionary:(NSDictionary *)rawRegion
{
    Region *region = [[[Region alloc] init] autorelease];
    id rawObject = [rawRegion objectForKey:kRegionIdKey];
    NSNumber *regionId = [rawObject isKindOfClass:[NSNumber class]] ?
            rawObject : nil;
    
    [region setRegionId:regionId];

    rawObject = [rawRegion objectForKey:kRegionNameKey];
    NSString *name = [rawObject isKindOfClass:[NSString class]] ?
            rawObject : nil;
    
    [region setName:name];
    return region;
}

+ (id)regionFromDictionary:(NSDictionary *)rawRegion
{
    Region *region = [self shortRegionFromDictionary:rawRegion];
    id rawObject = [rawRegion objectForKey:kRegionCountKey];
    NSNumber *count = [rawObject isKindOfClass:[NSNumber class]] ?
            rawObject : nil;
    
    [region setCount:count];

    rawObject = [rawRegion objectForKey:kRegionSuggestedKey];
    NSNumber *suggested = [rawObject isKindOfClass:[NSNumber class]] ?
            rawObject : nil;
    
    [region setSuggested:suggested];
    return region;
}
@end

#pragma mark -
#pragma mark RegionCollection
#pragma mark -

@implementation RegionCollection

#pragma mark -
#pragma mark NSObject

- (id)init
{
    if ((self = [super init]) != nil)
        _regions = [[NSMutableArray alloc] init];
    return self;
}

- (void)dealloc
{
    [_regions release];
    [super dealloc];
}

#pragma mark -
#pragma mark NSObject (NSKeyValueCoding)

@synthesize regions = _regions;

- (void)insertObject:(Region *)region inRegionsAtIndex:(NSUInteger)index
{
    [_regions insertObject:region atIndex:index];
}

- (void)insertRegions:(NSArray *)regions atIndexes:(NSIndexSet *)indexes
{
    [_regions insertObjects:regions atIndexes:indexes];
}

- (void)removeObjectFromRegionsAtIndex:(NSUInteger)index
{
    [_regions removeObjectAtIndex:index];
}

- (void)removeRegionsAtIndexes:(NSIndexSet *)indexes
{
    [_regions removeObjectsAtIndexes:indexes];
}

#pragma mark -
#pragma mark RegionCollection

+ (id)regionCollectionFromDictionary:(NSDictionary *)rawCollection
{
    RegionCollection *collection = [[[RegionCollection alloc] init]
            autorelease];
    NSMutableArray *mutableRegions = [collection mutableArrayValueForKey:
            kMutableRegionsKey];
    id rawObject = [rawCollection objectForKey:kRegionCollectionRegionsKey];
    NSArray *regions = [rawObject isKindOfClass:[NSArray class]] ?
            rawObject : nil;

    for (NSDictionary *rawRegion in regions) {
        Region *region = [Region regionFromDictionary:rawRegion];
        
        [mutableRegions addObject:region];
    }
    return collection;
}

- (void)copyPropertiesFromRegionCollection:(RegionCollection *)collection
{
    NSMutableArray *mutableRegions = [self mutableArrayValueForKey:
            kMutableRegionsKey];
    
    [mutableRegions addObjectsFromArray:[collection regions]];
}

#pragma mark -
#pragma mark <TTModel>

- (void)load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more
{
    if (![self isLoading]) {
        TTURLRequest *request = [TTURLRequest requestWithURL:kRegionListEndPoint
                delegate:self];
        
        ADD_DEFAULT_CACHE_POLICY_TO_REQUEST(request, cachePolicy);
        [request setResponse:[[[TTURLJSONResponse alloc] init] autorelease]];
        [request send];
    }
}

#pragma mark -
#pragma mark <TTURLRequestDelegate>

- (void)requestDidFinishLoad:(TTURLRequest *)request
{
    NSDictionary *rootObject = [(TTURLJSONResponse *)[request response]
            rootObject];
    RegionCollection *collection = [RegionCollection
            regionCollectionFromDictionary:rootObject];
    
    if (collection == nil) {
        [self didFailLoadWithError:BACKEND_ERROR([request urlPath], rootObject)
                tryAgain:NO];
        return;
    }
    [self copyPropertiesFromRegionCollection:collection];
    [super requestDidFinishLoad:request];
}
@end

#pragma mark -
#pragma mark MapAnnotation
#pragma mark -

@implementation MapAnnotation

#pragma mark -
#pragma mark MapAnnotation

@synthesize coordinate = _coordinate,
            title = _title,
            subtitle = _subtitle,
            pictureURL = _pictureURL,
            storeId = _storeId;

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate
{
    self = [self init];
    
    if (self != nil) {
        _coordinate = coordinate;
    }
    return  self;
}
@end

#pragma mark -
#pragma mark Store
#pragma mark -

@implementation Store

@synthesize storeId = _storeId,
            name = _name,
            storeAddress = _storeAddress,
            pictureURL = _pictureURL,
            code = _code,
            attendance = _attendance,
            region = _region,
            subregion = _subregion,
            district = _district,
            ubigeo = _ubigeo,
            phones = _phones,
            latitude = _latitude,
            longitude = _longitude;

#pragma mark -
#pragma mark Store

+ (id)shortStoreFromDictionary:(NSDictionary *)rawStore 
       whithLatitudeInLocation:(BOOL)latitudeInLocation;
{
    Store *store = [[[Store alloc] init] autorelease];
    id rawObject = [rawStore objectForKey:kStoreIdKey];
    NSNumber *storeId = [rawObject isKindOfClass:[NSNumber class]] ?
            rawObject : nil;
    
    [store setStoreId:storeId];

    if (!latitudeInLocation) {
        rawObject = [rawStore objectForKey:kStoreLatitudeKey];
        NSNumber *latitude = [rawObject isKindOfClass:[NSNumber class]] ?
            rawObject : nil;
        
        [store setLatitude:latitude];

        rawObject = [rawStore objectForKey:kStoreLongitudeKey];
        NSNumber *longitude = [rawObject isKindOfClass:[NSNumber class]] ?
            rawObject : nil;
        
        [store setLongitude:longitude];
    }
    rawObject = [rawStore objectForKey:kStoreNameKey];
    NSString *name = [rawObject isKindOfClass:[NSString class]] ?
            rawObject : nil;
    
    [store setName:name];

    rawObject = [rawStore objectForKey:kStoreAdressKey];
    NSString *address = [rawObject isKindOfClass:[NSString class]] ?
            rawObject : nil;
    
    [store setStoreAddress:address];

    rawObject = [rawStore objectForKey:kStorePictureURLKey];
    NSString *pictureURL = [rawObject isKindOfClass:[NSString class]] ?
            rawObject : nil;
    
    [store setPictureURL:[NSURL URLWithString:pictureURL]];
    return store;
}

+ (id)storeFromDictionary:(NSDictionary *)rawStore
{
    Store *store = [self shortStoreFromDictionary:rawStore 
            whithLatitudeInLocation:YES];
    id rawObject = [rawStore objectForKey:kStoreCodeKey];
    NSString *code = [rawObject isKindOfClass:[NSString class]] ?
            rawObject : nil;
    
    [store setCode:code];

    rawObject = [rawStore objectForKey:kStoreAttendanceKey];
    NSString *attendance = [rawObject isKindOfClass:[NSString class]] ?
            rawObject : nil;
    
    [store setAttendance:attendance];

    rawObject = [rawStore objectForKey:kStoreLocationKey];
    NSDictionary *rawLocation = [rawObject isKindOfClass:[NSDictionary class]] ?
            rawObject : nil;
    if (rawLocation) {
        rawObject = [rawLocation objectForKey:kStoreLatitudeKey];
        NSNumber *latitude = [rawObject isKindOfClass:[NSNumber class]] ?
                rawObject : nil;
        
        [store setLatitude:latitude];

        rawObject = [rawLocation objectForKey:kStoreLongitudeKey];
        NSNumber *longitude = [rawObject isKindOfClass:[NSNumber class]] ?
                rawObject : nil;
        
        [store setLongitude:longitude];
        
        rawObject = [rawLocation objectForKey:kStoreUbigeoKey];
        NSString *ubigeo = [rawObject isKindOfClass:[NSString class]] ?
                rawObject : nil;
        
        [store setUbigeo:ubigeo];
        
        Region *region = [Region shortRegionFromDictionary:[rawLocation
                objectForKey:kStoreRegionKey]];
        
        [store setRegion:region];
        
        Subregion *subregion = [Subregion subregionFromDictionary:[rawLocation
                objectForKey:kStoreSubregionKey]];
        
        [store setSubregion:subregion];

        Subregion *district = [Subregion subregionFromDictionary:
                [rawLocation objectForKey:kStoreDisctrictKey]];
        
        [store setDistrict:district];
    }
    rawObject = [rawStore objectForKey:kStorePhonesKey];
    NSString *phones = [rawObject isKindOfClass:[NSString class]] ?
        rawObject : nil;
    rawObject = [rawStore objectForKey:kStoreServicesKey];
    
    [store setPhones:phones];
    return store;
}

- (id)initWithStoreId:(NSString *)storeId
{
    self = [self init];
    
    if(self != nil) {
        _storeId = [NSNumber numberWithInteger:[storeId integerValue]];
    }
    return self;
}

- (void)copyPropertiesFromStore:(Store *)store
{
    [self setStoreId:[store storeId]];
    [self setCode:[store code]];
    [self setName:[store name]];
    [self setStoreAddress:[store storeAddress]];
    [self setAttendance:[store attendance]];
    [self setPictureURL:[store pictureURL]];
    [self setPhones:[store phones]];
    [self setRegion:[store region]];
    [self setSubregion:[store subregion]];
    [self setDistrict:[store district]];
    [self setUbigeo:[store ubigeo]];
    [self setLatitude:[store latitude]];
    [self setLongitude:[store longitude]];
}

#pragma mark -
#pragma mark <TTModel>

- (void)load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more
{
    if (![self isLoading]) {
        TTURLRequest *request = [TTURLRequest requestWithURL:
                URL(kStoreDetailEndPoint, _storeId) delegate:self]; 

        ADD_DEFAULT_CACHE_POLICY_TO_REQUEST(request, cachePolicy);
        [request setResponse:[[[TTURLJSONResponse alloc] init] autorelease]];
        [request send];
    }
}

#pragma mark -
#pragma mark <TTURLRequestDelegate>

- (void)requestDidFinishLoad:(TTURLRequest *)request
{
    NSDictionary *rootObject = [(TTURLJSONResponse *)[request response]
            rootObject];
    Store *store = [Store storeFromDictionary:rootObject];
    
    if (store == nil) {
        [self didFailLoadWithError:BACKEND_ERROR([request urlPath], rootObject)
                tryAgain:NO];
        return;
    }
    [self copyPropertiesFromStore:store];
    [super requestDidFinishLoad:request];
}
@end

#pragma mark -
#pragma mark StoreCollection
#pragma mark -

@implementation StoreCollection

@synthesize regionId = _regionId,
            subregionId = _subregionId,
            stores = _stores,
            districts = _districts;

#pragma mark -
#pragma mark NSObject

- (id)init
{
    self = [super init];
    
    if (self != nil) {
        _stores = [[NSMutableArray alloc] init];
        _districts = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [_stores release];
    [_districts release];
    [super dealloc];
}

#pragma mark -
#pragma mark NSObject (NSKeyValueCoding)

- (void)insertObject:(Store *)store inStoresAtIndex:(NSUInteger)index
{
    [_stores insertObject:store atIndex:index];
}

- (void)insertStores:(NSArray *)stores atIndexes:(NSIndexSet *)indexes
{
    [_stores insertObjects:stores atIndexes:indexes];
}

- (void)removeObjectFromStoresAtIndex:(NSUInteger)index
{
    [_stores removeObjectAtIndex:index];
}

- (void)removeStoresAtIndexes:(NSIndexSet *)indexes
{
    [_stores removeObjectsAtIndexes:indexes];
}

- (void)insertObject:(NSString *)district inDistrictsAtIndex:(NSUInteger)index
{
    [_districts insertObject:district atIndex:index];
}

- (void)insertDistricts:(NSArray *)districts
              atIndexes:(NSIndexSet *)indexes
{
    [_districts insertObjects:districts atIndexes:indexes];
}

- (void)removeObjectFromDistrictsAtIndex:(NSUInteger)index
{
    [_districts removeObjectAtIndex:index];
}

- (void)removeDistrictsAtIndexes:(NSIndexSet *)indexes
{
    [_districts removeObjectsAtIndexes:indexes];
}

#pragma mark -
#pragma mark StoreCollection

+ (id)storeCollectionFromDictionary:(NSDictionary *)rawCollection
{
    StoreCollection *collection = [[[StoreCollection alloc] init] autorelease];
    NSMutableArray *mutableStores = [collection mutableArrayValueForKey:
            kMutableStoresKey];
    NSMutableArray *mutableDistricts = [collection mutableArrayValueForKey:
            kMutableStoreTitlesKey];
    id rawObject = [rawCollection objectForKey:kStoreCollectionDistrictsKey];
    NSArray *districts = [rawObject isKindOfClass:[NSArray class]] ?
            rawObject : nil;
    
    for (NSDictionary *district in districts) {
        rawObject = [district objectForKey:kStoreCollectionStoresKey];
        NSArray *rawStores = [rawObject isKindOfClass:[NSArray class]] ?
                rawObject : nil;
        
        NSMutableArray *storesInDistrict = [NSMutableArray array];
        
        for (NSDictionary *rawStore in rawStores) {
            Store *store = [Store shortStoreFromDictionary:rawStore
                    whithLatitudeInLocation:NO];
            
            [storesInDistrict addObject:store];
        }
        [mutableStores addObject:storesInDistrict];
        rawObject = [district objectForKey:kStoreCollectionNameKey];
        NSString *sectionName = [rawObject isKindOfClass:[NSString class]] ?
                rawObject : nil;
        
        [mutableDistricts addObject:sectionName];
    }
    return collection;
}

- (id)initWithSubregionId:(NSString *)subregionId
              andRegionId:(NSString *)regionId
{
    self = [self init];
    
    if (self != nil) {
        _subregionId = [NSNumber numberWithInteger:[subregionId integerValue]];
        _regionId = [NSNumber numberWithInteger:[regionId integerValue]];
    }
    return self;
}

- (void)copyPropertiesFromStoreCollection:(StoreCollection *)collection;
{
    NSMutableArray *stores = [self mutableArrayValueForKey:kMutableStoresKey];
    
    [stores addObjectsFromArray:[collection stores]];
    
    NSMutableArray *storeTitles = [self mutableArrayValueForKey:
            kMutableStoreTitlesKey];
    
    [storeTitles addObjectsFromArray:[collection districts]];
}

#pragma mark -
#pragma mark <TTModel>

- (void)load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more
{
    if (![self isLoading]) {
        TTURLRequest *request = [TTURLRequest requestWithURL:
                URL(kStoreListEndPoint, _regionId, _subregionId) delegate:self]; 
        
        ADD_DEFAULT_CACHE_POLICY_TO_REQUEST(request, cachePolicy);
        [request setResponse:[[[TTURLJSONResponse alloc] init] autorelease]];
        [request send];
    }
}

#pragma mark -
#pragma mark <TTURLRequestDelegate>

- (void)requestDidFinishLoad:(TTURLRequest *)request
{
    NSDictionary *rootObject = [(TTURLJSONResponse *)[request response]
            rootObject];
    StoreCollection *collection = [StoreCollection
            storeCollectionFromDictionary:rootObject];
    
    if (collection == nil) {
        [self didFailLoadWithError:BACKEND_ERROR([request urlPath], rootObject)
                tryAgain:NO];
        return;
    }
    [self copyPropertiesFromStoreCollection:collection];
    [super requestDidFinishLoad:request];
}
@end
