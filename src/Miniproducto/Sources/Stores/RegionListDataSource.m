#import "Common/Constants.h"
#import "Stores/Models.h"
#import "Stores/Constants.h"
#import "Stores/RegionListDataSource.h"

@implementation RegionListDataSource

#pragma mark -
#pragma mark NSObject

- (id)init
{
    self = [super init];
    
    if (self != nil) {
        [self setModel:[[[RegionCollection alloc] init] autorelease]];
    }
    return self;
}

#pragma mark -
#pragma mark RegionListDataSource (Public)

- (id)initWithRegionId:(NSString *)regionId
{
    self = [super init];
    
    if (self != nil) {
        [self setModel:[[[SubregionCollection alloc] initWithRegionId:regionId]
                autorelease]];
    }
    return self;
}

#pragma mark -
#pragma mark <TTTableViewDataSource>

- (void)tableViewDidLoadModel:(UITableView *)tableView
{
    if ([[self items] count] > 0) {
        return;
    }
    RegionCollection *regionCollection = (RegionCollection *)[self model];
    NSArray *regions = [regionCollection regions];
    NSMutableArray *items = [NSMutableArray arrayWithCapacity:[regions count]];
    
    for (Region *region in regions) {
        NSString *name = [region name];
        NSString *URL;
        
        if ([[region count] integerValue] == 1) {  
            URL = URL(kURLStoreListCall, [region suggested], [region regionId],
                    [region name]);
        } else {
            URL = URL(kURLSubregionListCall, [region regionId], [region name]);
        }
        TTTableImageItem *item = [TTTableImageItem itemWithText:name imageURL:
                    @"bundle://stores_icon.png" URL:URL];
        
        [items addObject:item];
    }
    [self setItems:items];
}
@end