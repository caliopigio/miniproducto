#import "Common/Controllers/Three20/ReconnectableTableViewController.h"
#import "Contact/ContactDetailDataSource.h"

@interface ContactDetailController : ReconnectableTableViewController
        <ContactDetailDataSourceDelegate>

@end
