// JSON keys
extern NSString *const kContactInfoKey;
extern NSString *const kContactPictureURLKey;

// Controllers URLs
extern NSString *const kURLContactDetailController;

// Controllers URL calls
extern NSString *const kURLContactDetailControllerCall;

// Endpoints URLs
extern NSString *const kContactDetailEndpoint;
