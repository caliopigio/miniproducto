#import "Common/Constants.h"
#import "Contact/Constants.h"

// JSON keys
NSString *const kContactInfoKey = @"info";
NSString *const kContactPictureURLKey = @"picture_url";

// Controllers URLs
NSString *const kURLContactDetailController = @"tt://launcher/contact/detail/";

// Controllers URL calls
NSString *const kURLContactDetailControllerCall =
        @"tt://launcher/contact/detail/";

// Endpoints URL
NSString *const kContactDetailEndpoint = ENDPOINT(@"/contact/details.json");
