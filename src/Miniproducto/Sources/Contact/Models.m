#import "Three20/Three20.h"
#import "extThree20JSON/extThree20JSON.h"

#import "Common/Constants.h"
#import "Contact/Constants.h"
#import "Contact/Models.h"

#pragma mark -
#pragma mark Contact
#pragma mark -

@implementation Contact

@synthesize pictureURL = _pictureURL,
            info = _info;

#pragma mark -
#pragma mark Contact

+ (id)contactFromDictionary:(NSDictionary *)rawContact
{
    Contact *contact = [[[Contact alloc] init] autorelease];
    id rawObject = [rawContact objectForKey:kContactInfoKey];
    NSString *info = [rawObject isKindOfClass:[NSString class]] ?
            rawObject : nil;
    
    [contact setInfo:info];
    
    rawObject = [rawContact objectForKey:kContactPictureURLKey];
    NSString *pictureURL = [rawObject isKindOfClass:[NSString class]] ?
            rawObject : nil;
    
    [contact setPictureURL:[NSURL URLWithString:pictureURL]];
    return contact;
}

- (void)copyPropertiesFromContact:(Contact *)contact
{
    [self setInfo:[contact info]];
    [self setPictureURL:[contact pictureURL]];
}

#pragma mark -
#pragma mark <TTModel>

- (void)load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more
{
    if (![self isLoading]) {
        TTURLRequest *request = [TTURLRequest requestWithURL:
                kContactDetailEndpoint delegate:self];
        
        ADD_DEFAULT_CACHE_POLICY_TO_REQUEST(request, cachePolicy);
        [request setResponse:[[[TTURLJSONResponse alloc] init] autorelease]];
        [request send];
    }
}

#pragma mark -
#pragma mark <TTURLRequestDelegate>

- (void)requestDidFinishLoad:(TTURLRequest *)request
{
    NSDictionary *rootObject = [(TTURLJSONResponse *)[request response]
            rootObject];
    Contact *contact = [Contact contactFromDictionary:rootObject];
    
    [self copyPropertiesFromContact:contact];
    [super requestDidFinishLoad:request];
}
@end
