#import "Common/Additions/TTStyleSheet+Additions.h"
#import "Common/Constants.h"
#import "Contact/Constants.h"
#import "Contact/Models.h"
#import "Contact/ContactDetailDataSource.h"

@implementation ContactDetailDataSource

@synthesize delegate = _delegate;

#pragma mark -
#pragma NSObject

- (id)init
{
    self = [super init];
    
    if (self != nil) {
        [self setModel:[[[Contact alloc] init] autorelease]];
    }
    return self;
}

#pragma mark -
#pragma mark ContactDetailDataSource

- (id)initWithDelegate:(id<ContactDetailDataSourceDelegate>)delegate
{
    self = [self init];
    
    if (self != nil) {
        _delegate = delegate;
    }
    return self;
}

#pragma mark -
#pragma mark <TTTableViewDataSource>

- (void)    tableView:(UITableView *)tableView
                 cell:(UITableViewCell *)cell
willAppearAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor colorWithRed:223./255. green:217./255.
            blue:212./255. alpha:1.]];
    
    UILabel *textLabel = [cell textLabel];
    
    [textLabel setBackgroundColor:[UIColor clearColor]];
    [textLabel setTextColor:[UIColor colorWithRed:0./255. green:99./255.
            blue:54./255. alpha:1.]];
    [textLabel setShadowColor:[UIColor whiteColor]];
    [textLabel setShadowOffset:CGSizeMake(.0, 1.)];
}

- (void)tableViewDidLoadModel:(UITableView *)tableView
{
    if ([[self items] count] > 0) {
        return;
    }
    Contact *contact = (Contact *)[self model];
    
    if ([_delegate respondsToSelector:@selector(dataSource:viewForHeader:)]) {
        CGRect frame = CGRectMake(.0, .0, 320., 160.);
        TTImageView *banner = [[TTImageView alloc] initWithFrame:frame];
        
        [banner setUrlPath:[[contact pictureURL] absoluteString]];
        [_delegate dataSource:self viewForHeader:banner];
    }
    TTTableLongTextItem *item = [TTTableLongTextItem itemWithText:
            [contact info]];
    
    [self setItems:[NSMutableArray arrayWithObject:item]];
}
@end
