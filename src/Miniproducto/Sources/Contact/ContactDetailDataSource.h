#import "Common/Controllers/BaseListDataSource.h"

@protocol ContactDetailDataSourceDelegate;

@interface ContactDetailDataSource: BaseListDataSource
{
    id<ContactDetailDataSourceDelegate> _delegate;
}
@property (nonatomic, assign) id delegate;

- (id)initWithDelegate:(id<ContactDetailDataSourceDelegate>)delegate;
@end

@protocol ContactDetailDataSourceDelegate <NSObject>

@optional
- (void)dataSource:(ContactDetailDataSource *)dataSource
     viewForHeader:(UIView *)view;
@end
