#import "Common/Additions/TTStyleSheet+Additions.h"
#import "Common/Constants.h"
#import "Contact/ContactDetailController.h"

@implementation ContactDetailController

#pragma mark -
#pragma mark UIViewController

- (id) initWithNibName:(NSString *)nibNameOrNil
                bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self != nil) {
        [self setTableViewStyle:UITableViewStyleGrouped];
        [[self tableView] setBackgroundColor:[UIColor colorWithPatternImage:
                TTIMAGE(@"bundle://background_texture.png")]];
        
        TTImageView *background = [[TTImageView alloc] initWithFrame:
                [[self view] frame]];
        
        [background setUrlPath:@"bundle://background_texture.png"];
        [[self tableView] setBackgroundView:background];
        [self setVariableHeightRows:YES];
        [[self tableView] setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [self setTitle:@"Nosotros"];
    }
    return self;
}

#pragma mark -
#pragma mark TTTableViewController

- (void)createModel
{
    [self setDataSource:[[[ContactDetailDataSource alloc]
            initWithDelegate:self] autorelease]];
}

#pragma mark -
#pragma mark <ContactDetailDataSourceDelegate>

- (void)dataSource:(ContactDetailDataSource *)dataSource
     viewForHeader:(UIView *)view
{
    [[self tableView] setTableHeaderView:view];
}
@end
