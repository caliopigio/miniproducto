#import "Common/Models/URLRequestModel.h"

#pragma mark -
#pragma mark Contact
#pragma mark -

@interface Contact : URLRequestModel
{
    NSURL *_pictureURL;
    NSString *_info;
}
@property (nonatomic, retain) NSURL *pictureURL;
@property (nonatomic, retain) NSString *info;

+ (id)contactFromDictionary:(NSDictionary *)rawContact;

- (void)copyPropertiesFromContact:(Contact *)contact;
@end
