#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import <Three20/Three20.h>

#import "Launcher/Models.h"
#import "CarouselViewController.h"

@protocol ConnectedCarouselDelegate;

@interface ConnectedCarouselViewController : CarouselViewController
        <TTImageViewDelegate>
{
    id<ConnectedCarouselDelegate> _delegate;
}
@property (nonatomic, assign) id<ConnectedCarouselDelegate> delegate;

- (id)initWithDelegate:(id<ConnectedCarouselDelegate>)delegate;
- (void)addViewWithURLToCarousel:(NSString *)URL;
- (void)addViewFromBanner:(Banner *)banner;
@end

@protocol ConnectedCarouselDelegate <NSObject>

@optional
- (void)carousel:(ConnectedCarouselViewController *)carousel
    didLoadImage:(BannerImage *)image;

@end
