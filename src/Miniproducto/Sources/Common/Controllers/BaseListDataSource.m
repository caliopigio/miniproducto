#import "Common/Constants.h"
#import "Common/Controllers/Constants.h"
#import "BaseListDataSource.h"

@implementation BaseListDataSource

#pragma mark -
#pragma mark <TTTableViewDataSource>

- (NSString *)titleForLoading:(BOOL)reloading
{
    return NSLocalizedString(@"Recolectando información...", nil);
}

- (NSString *)titleForError:(NSError *)error
{
    return NSLocalizedString(@"Ha ocurrido un error :(", nil);
}

- (NSString *)subtitleForError:(NSError *)error
{
    return NSLocalizedString(@"Por favor intente de nuevo más tarde", nil);
}

- (NSString *)titleForEmpty
{
    return NSLocalizedString(@"No hay información", nil);
}

- (NSString *)subtitleForEmpty
{
    return NSLocalizedString(@"Por favor intente de nuevo más tarde", nil);
}

- (void)    tableView:(UITableView *)tableView
                 cell:(UITableViewCell *)cell
willAppearAtIndexPath:(NSIndexPath *)indexPath
{
    UIImage *rawImage = TTIMAGE(@"bundle://cellBackground.png");
    UIImage *backgroundImage = [rawImage resizableImageWithCapInsets:
            UIEdgeInsetsMake(21., .0, 21., .0)];
    
    UIImageView *background = [[UIImageView alloc]
            initWithImage:backgroundImage];
    
    [cell setBackgroundView:background];
    
    UILabel *textLabel = [cell textLabel];
    
    [textLabel setBackgroundColor:[UIColor clearColor]];
    [textLabel setFont:[UIFont boldSystemFontOfSize:20.]];
    [textLabel setTextColor:[UIColor colorWithRed:0./255. green:99./255.
            blue:54./255. alpha:1.]];
    [textLabel setShadowColor:[UIColor whiteColor]];
    [textLabel setShadowOffset:CGSizeMake(.0, 1.)];
}
@end
