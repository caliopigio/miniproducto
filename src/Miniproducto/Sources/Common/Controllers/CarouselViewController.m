#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <QuartzCore/CALayer.h>

#import "Common/Controllers/CarouselViewController.h"

static const CGFloat kDelay = 4.;
static const CGFloat kImageHeight = 150.;

@interface CarouselViewController (Private)

- (UIImageView *)previousView;
- (UIImageView *)nextView;
- (void)autoscroll;
- (NSInteger)previousIndex;
- (NSInteger)nextIndex;
@end

@implementation CarouselViewController

#pragma mark -
#pragma mark NSObject

- (id)init
{
    self = [super init];
    
    if (self != nil) {
        _carouselWidth = [[self view] frame].size.width * 2.;
        _trigger = [NSTimer scheduledTimerWithTimeInterval:kDelay target:self
                selector:@selector(autoscroll) userInfo:nil repeats:YES];
    }
    return self;
}

#pragma mark -
#pragma mark UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self updateCarousel];
}

#pragma mark -
#pragma mark CarouselViewController

@synthesize views = _views, currentView = _currentView, carousel = _carousel,
        carouselHeight = _carouselHeight;

- (void)setCarouselHeight:(CGFloat)height
{
    _carouselHeight = height;
    CGRect frame = [[self view] frame];
    frame.size.height = height;
    [[self view] setFrame:frame];
}

- (id)initWithViews:(NSArray *)views
{
    self = [self init];
    _views = (NSMutableArray *)views;
    
    return self;
}

- (void)addViewToCarousel:(UIView *)view
{
    if (_views == nil) {
        _views = [[NSMutableArray alloc] init];
    }
    if ([view isKindOfClass:[UIView class]]) {
        [_views addObject:view];
    }
    [self updateCarousel];
}

- (void)updateCarousel
{
    for (UIView *view in [_carousel subviews]) {
        [view removeFromSuperview];
    }
    if ([_views count] > 0) {
        [_pageControl setNumberOfPages:[_views count]];
        [_pageControl setCurrentPage:_currentView];
        
        UIScrollView *carousel = [self carousel];
        
        if ([_views count] > 1) {
            UIView *previousView = (UIView *)[self previousView];
            CGRect frame = previousView.frame;
            frame.origin.x = 0;
            
            [previousView setFrame:frame];
            [carousel addSubview:previousView];
            
            UIView *currentView = (UIView *)[_views objectAtIndex:_currentView];
            frame = currentView.frame;
            frame.origin.x = frame.size.width;
            
            [currentView setFrame:frame];
            [carousel addSubview:currentView];
            
            UIView *nextView = (UIView *)[self nextView];
            frame = nextView.frame;
            frame.origin.x = (frame.size.width * 2.);
            
            [nextView setFrame:frame];
            [carousel addSubview:nextView];
            
            frame.size.width = (frame.size.width * 3);
            
            [carousel setContentSize:frame.size];
            [carousel setContentOffset:
                    CGPointMake(_carousel.frame.size.width, .0)];
        } else {
            UIView *currentView = (UIView *)[_views objectAtIndex:_currentView];
            CGRect frame = currentView.frame;
            frame.origin.x = 0;
            
            [currentView setFrame:frame];
            [carousel addSubview:currentView];
            [carousel setContentSize:frame.size];
            [carousel setContentOffset:CGPointMake(.0, .0)];
        }
    }
}

- (UIScrollView *)carousel
{
    if (_carousel == nil) {
        CGRect frame = [[UIScreen mainScreen] bounds];
        frame.size.height = _carouselHeight ? _carouselHeight : kImageHeight;
        _carousel = [[UIScrollView alloc] initWithFrame:frame];
        
        [_carousel setContentSize:CGSizeMake
         (_carousel.frame.size.width * 3., _carousel.frame.size.height)];
        [_carousel setContentOffset:
         CGPointMake(_carousel.frame.size.width, .0)];
        [_carousel setAlwaysBounceHorizontal:YES];
        [_carousel setPagingEnabled:YES];
        [_carousel setDelegate:self];
        [[self view] addSubview:_carousel];
        
        frame.origin.y = frame.size.height - 20.;
        frame.size.height = 20.;
        _pageControl = [[UIPageControl alloc] initWithFrame:frame];
        
        [[self view] addSubview:_pageControl];
    }
    return _carousel;
}

#pragma mark -
#pragma CarouselViewController (Private)

- (UIImageView *)previousView
{
    UIView *view = [_views objectAtIndex:[self previousIndex]];
    CGSize size = [view frame].size;
    
    UIGraphicsBeginImageContextWithOptions(size, NO, .0);
    [[view layer] renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    
    UIGraphicsEndImageContext();
    return imageView;
}

- (UIImageView *)nextView
{
    UIView *view = [_views objectAtIndex:[self nextIndex]];
    CGSize size = [view frame].size;
    
    UIGraphicsBeginImageContextWithOptions(size, NO, .0);
    [[view layer] renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    
    UIGraphicsEndImageContext();
    return imageView;
}

- (NSInteger)previousIndex
{
    return (_currentView == 0) ?
            ([_views count] - 1) : (_currentView - 1);
}

- (NSInteger)nextIndex
{
    return (_currentView == ([_views count] - 1)) ?
            0 : (_currentView + 1);
}

- (void)autoscroll
{
    if ([_views count] > 1) {
        [_carousel setContentOffset:CGPointMake(_carouselWidth, .0)
                animated:YES];
    }
}

#pragma mark -
#pragma mark <UIScrollViewDelegate>

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [_trigger invalidate];
    
    int offset = [scrollView contentOffset].x;
    
    if (offset == 0) {
        _currentView = [self previousIndex];
    } else if (offset == _carouselWidth) {
        _currentView = [self nextIndex];
    }
    [self updateCarousel];
    
    _trigger = [NSTimer scheduledTimerWithTimeInterval:kDelay target:self
            selector:@selector(autoscroll) userInfo:nil repeats:YES];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    int offset = [scrollView contentOffset].x;
    
    if (offset == 0) {
        _currentView = [self previousIndex];
    } else if (offset == _carouselWidth) {
        _currentView = [self nextIndex];
    }
    [self updateCarousel];
}
@end
