#import "BaseTableViewController.h"

@implementation BaseTableViewController

- (id)initWithNibName:(NSString *)nibName bundle:(NSBundle *)nibBundle
{
    self = [super initWithNibName:nibName bundle:nibBundle];
    
    if (self) {
        [[self tableView] setBackgroundColor:[UIColor colorWithPatternImage:
                TTIMAGE(@"bundle://background_texture.png")]];
        [self setStatusBarStyle:UIStatusBarStyleBlackOpaque];
        [self setVariableHeightRows:YES];
        [[self tableView] setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    }
    return self;
}
@end
