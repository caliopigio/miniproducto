#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CarouselViewController : UIViewController <UIScrollViewDelegate>
{
    NSTimer *_trigger;
    UIPageControl *_pageControl;
    CGFloat _carouselWidth;
    CGFloat _carouselHeight;
    NSMutableArray *_views;
    NSInteger _currentView;
    UIScrollView *_carousel;
}
@property (nonatomic, retain) NSMutableArray *views;
@property (nonatomic, assign) NSInteger currentView;
@property (nonatomic, retain) UIScrollView *carousel;
@property (nonatomic, assign) CGFloat carouselHeight;

- (id)initWithViews:(NSArray *)views;
- (void)addViewToCarousel:(UIView *)view;
- (void)updateCarousel;
@end

