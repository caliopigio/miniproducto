#import "Launcher/Models.h"
#import "ConnectedCarouselViewController.h"

@implementation ConnectedCarouselViewController

@synthesize delegate = _delegate;

#pragma mark -
#pragma mark ConnectedCarouselViewController

- (id)initWithDelegate:(id<ConnectedCarouselDelegate>)delegate
{
    self = [super init];
    
    if (self != nil) {
        _delegate = delegate;
    }
    return self;
}

- (void)addViewWithURLToCarousel:(NSString *)URL
{
    CGRect frame = [[self view] frame];
    frame.size = CGSizeMake(_carouselWidth / 2., _carouselHeight);
    TTImageView *banner = [[TTImageView alloc] initWithFrame:frame];
    
    [banner setUrlPath:URL];
    [banner setDelegate:self];
}

- (void)addViewFromBanner:(Banner *)banner
{
    CGRect frame = [[self view] frame];
    frame.size = CGSizeMake(_carouselWidth / 2., _carouselHeight);
    BannerImage *bannerImage = [[BannerImage alloc] initWithFrame:frame];
    NSString *url = [NSString stringWithFormat:@"%@?width=%.0f",
            [[banner pictureURL] absoluteString],
                [[self view] frame].size.width * 2];
    
    [bannerImage setBanner:banner];
    [bannerImage setUrlPath:url];
    [bannerImage setDelegate:self];
}

#pragma mark -
#pragma mark <TTImageViewDelegate>

- (void)imageViewDidStartLoad:(TTImageView *)imageView
{
    
}

- (void)   imageView:(TTImageView *)imageView
didFailLoadWithError:(NSError *)error
{
    
}

- (void)imageView:(TTImageView *)imageView didLoadImage:(UIImage *)image
{
    [self addViewToCarousel:imageView];
    if ([_delegate respondsToSelector:@selector(carousel:didLoadImage:)]) {
        [_delegate carousel:self didLoadImage:(BannerImage *)imageView];
    }
}
@end
