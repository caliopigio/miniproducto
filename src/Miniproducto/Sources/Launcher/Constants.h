// LauncherViewController's constants

extern NSString *const kLauncherTitle;

// toolbar delay

extern const NSTimeInterval kLauncherToolbarDelay;

// Controller URLs

extern NSString *const kURLLauncher;

// Controller URL's calls

extern NSString *const kURLLauncherCall;

// Launcher item's icons

extern NSString *const kURLShoppingListIcon;
extern NSString *const kURLRecipesIcon;
extern NSString *const kURLOffersIcon;
extern NSString *const kURLStoresIcon;
extern NSString *const kURLBodyMeterIcon;
extern NSString *const kURLSomelierIcon;
extern NSString *const kURLCompositionIcon;
extern NSString *const kURLPhonesIcon;

// Module model's constants

extern NSString *const kModuleNameKey;
extern NSString *const kModulePictureKey;

// Banner model's constants

extern NSString *const kBannerIdKey;
extern NSString *const kBannerCodeKey;
extern NSString *const kBannerNameKey;
extern NSString *const kBannerPictureURLKey;

// BannerCollection model's constants

extern NSString *const kBannerCollectionBannersKey;

// Endpoints

extern NSString *const kBannerListEndpoint;