#import "Common/Controllers/ConnectedCarouselViewController.h"
#import "Common/Controllers/BaseTableViewController.h"

@interface ListLauncherController : BaseTableViewController
        <TTModelDelegate, ConnectedCarouselDelegate>
{
    ConnectedCarouselViewController *_carousel;

}

@end
