#import <Three20/Three20.h>
#import <extThree20JSON/extThree20JSON.h>

#import "Common/Constants.h"
#import "Launcher/Constants.h"
#import "Launcher/Models.h"

static NSString *const kMutablePromotionsKey = @"promotions";
static NSString *const kMutableBannersKey = @"banners";
static NSString *const kMutableModulesKey = @"modules";

#pragma mark -
#pragma mark Module
#pragma mark -

@implementation Module

@synthesize name = _name,
            picture = _picture;

#pragma mark -
#pragma mark Module

+ (id)moduleFromDictionary:(NSDictionary *)rawModule
{
    Module *module = [[[Module alloc] init] autorelease];
    
    id rawObject = [rawModule objectForKey:kModuleNameKey];
    NSString *name = [rawObject isKindOfClass:[NSString class]] ?
            rawObject : nil;
    
    [module setName:name];

    rawObject = [rawModule objectForKey:kModulePictureKey];
    NSString *picture = [rawObject isKindOfClass:[NSString class]] ?
            rawObject : nil;
    
    [module setPicture:[NSURL URLWithString:picture]];
    return module;
}
@end

#pragma mark -
#pragma mark ModuleCollection
#pragma mark

@implementation ModuleCollection

@synthesize modules = _modules;

#pragma mark -
#pragma mark NSObject

- (id)init
{
    self = [super init];
    
    if (self != nil) {
        _modules = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [_modules release];
    [super dealloc];
}

#pragma mark -
#pragma mark NSObject (NSKeyValueCoding)

- (void)insertObject:(Module *)module
    inModulesAtIndex:(NSUInteger)index
{
    [_modules insertObject:module atIndex:index];
}

- (void)insertModules:(NSArray *)modules
            atIndexes:(NSIndexSet *)indexes
{
    [_modules insertObjects:modules atIndexes:indexes];
}

- (void)removeObjectFromModulesAtIndex:(NSUInteger)index
{
    [_modules removeObjectAtIndex:index];
}

- (void)removeModulesAtIndexes:(NSIndexSet *)indexes
{
    [_modules removeObjectsAtIndexes:indexes];
}

#pragma mark -
#pragma mark ModuleCollection

- (id)initWithModules:(NSArray *)modules
{
    self = [self init];
    
    if (self != nil) {
        NSMutableArray *mutableModules = [self mutableArrayValueForKey:
                kMutableModulesKey];
        
        [mutableModules addObjectsFromArray:modules];
    }
    return self;
}
@end

#pragma mark -
#pragma mark Banner
#pragma mark -

@implementation Banner

@synthesize bannerId = _bannerId,
            code = _code,
            name = _name,
            pictureURL = _pictureURL;

#pragma mark -
#pragma mark Banner

+ (id)bannerFromDictionary:(NSDictionary *)rawBanner
{
    Banner *banner = [[[Banner alloc] init] autorelease];
    id rawObject = [rawBanner objectForKey:kBannerIdKey];
    NSNumber *bannerId = [rawObject isKindOfClass:[NSNumber class]] ?
            rawObject : nil;
    
    [banner setBannerId:bannerId];

    rawObject = [rawBanner objectForKey:kBannerCodeKey];
    NSString *code = [rawObject isKindOfClass:[NSString class]] ?
            rawObject : nil;
    
    [banner setCode:code];

    rawObject = [rawBanner objectForKey:kBannerNameKey];
    NSString *name = [rawObject isKindOfClass:[NSString class]] ?
            rawObject : nil;
    
    [banner setName:name];

    rawObject = [rawBanner objectForKey:kBannerPictureURLKey];
    NSString *pictureURL = [rawObject isKindOfClass:[NSString class]] ?
            rawObject : nil;
    
    [banner setPictureURL:[NSURL URLWithString:pictureURL]];
    return banner;
}
@end

#pragma mark -
#pragma mark BannerCollection
#pragma mark

@implementation BannerCollection
        
@synthesize banners = _banners;

#pragma mark -
#pragma mark NSObject

- (id)init
{
    self = [super init];
    
    if (self != nil) {
        _banners = [[NSMutableArray alloc] init];
    }
    return self;
}

#pragma mark -
#pragma mark NSObject

- (void)dealloc
{
    [_banners release];
    [super dealloc];
}

#pragma mark -
#pragma mark NSObject (NSKeyValueCoding)

- (void)insertObject:(Banner *)banner inBannersAtIndex:(NSUInteger)index
{
    [_banners insertObject:banner atIndex:index];
}

- (void)insertBanners:(NSArray *)banners atIndexes:(NSIndexSet *)indexes
{
    [_banners insertObjects:banners atIndexes:indexes];
}

- (void)removeObjectFromBannersAtIndex:(NSUInteger)index
{
    [_banners removeObjectAtIndex:index];
}

- (void)removeBannersAtIndexes:(NSIndexSet *)indexes
{
    [_banners removeObjectsAtIndexes:indexes];
}

#pragma mark -
#pragma mark BannerCollection

+ (id)bannerCollectionFromDictionary:(NSDictionary *)rawCollection
{
    BannerCollection *collection = [[[BannerCollection alloc] init]
            autorelease];
    NSArray *banners = [rawCollection objectForKey:kBannerCollectionBannersKey];
    NSMutableArray *mutableBanners = [collection mutableArrayValueForKey:
            kMutableBannersKey];
    
    for (NSDictionary *rawBanner in banners) {
        Banner *banner = [Banner bannerFromDictionary:rawBanner];
        
        [mutableBanners addObject:banner];
    }
    return collection;
}

- (void)copyPropertiesFromPromotionCollection:(BannerCollection *)collection
{
    NSMutableArray *mutableBanners = [self mutableArrayValueForKey:
            kMutableBannersKey];
    
    [mutableBanners addObjectsFromArray:[collection banners]];
}

#pragma mark -
#pragma <TTModel>

- (void)load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more
{
    if (![self isLoading]) {
        TTURLRequest *request = [TTURLRequest requestWithURL:kBannerListEndpoint
                delegate:self];
        
        ADD_DEFAULT_CACHE_POLICY_TO_REQUEST(request, cachePolicy);
        [request setResponse:[[[TTURLJSONResponse alloc] init] autorelease]];
        [request send];
    }
}

#pragma mark
#pragma <TTURLRequestDelegate>

- (void)requestDidFinishLoad:(TTURLRequest *)request
{
    TTURLJSONResponse *response = [request response];
    NSDictionary *rootObject = [response rootObject];
    BannerCollection *collection = [BannerCollection
            bannerCollectionFromDictionary:rootObject];
    
    if (collection == nil) {
        [self didFailLoadWithError:BACKEND_ERROR([request urlPath], rootObject)
                tryAgain:NO];
    }
    [self copyPropertiesFromPromotionCollection:collection];
    [super requestDidFinishLoad:request];
}
@end

#pragma mark -
#pragma mark BannerImage
#pragma mark -

@implementation BannerImage

@synthesize banner = _banner;

@end
