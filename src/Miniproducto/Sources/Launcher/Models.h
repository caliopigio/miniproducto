#import "Common/Models/URLRequestModel.h"

#pragma mark -
#pragma mark Module
#pragma mark -

@interface Module : NSObject
{
    NSString *_name;
    NSURL *_picture;
}
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSURL *picture;

+ (id)moduleFromDictionary:(NSDictionary *)rawModule;
@end

#pragma mark -
#pragma mark ModuleCollection
#pragma mark -

@interface ModuleCollection : TTModel
{
    NSMutableArray *_modules;
}
@property (nonatomic, readonly) NSArray *modules;

- (id)initWithModules:(NSArray *)modules;
@end

#pragma mark -
#pragma mark Banner
#pragma mark -

@interface Banner : NSObject
{
    NSNumber *_bannerId;
    NSString *_code;
    NSString *_name;
    NSURL *_pictureURL;
}
@property (nonatomic, retain) NSNumber *bannerId;
@property (nonatomic, retain) NSString *code;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSURL *pictureURL;

+ (id)bannerFromDictionary:(NSDictionary *)rawBanner;
@end

#pragma mark -
#pragma mark BannerCollection
#pragma mark -

@interface BannerCollection : URLRequestModel
{
    NSMutableArray *_banners;
}
@property (nonatomic, readonly) NSArray *banners;

+ (id)bannerCollectionFromDictionary:(NSDictionary *)rawCollection;

- (void)copyPropertiesFromPromotionCollection:(BannerCollection *)collection;
@end

#pragma mark -
#pragma mark BannerImage
#pragma mark -

@interface BannerImage : TTImageView
{
    Banner *_banner;
}
@property (nonatomic, retain) Banner *banner;

@end