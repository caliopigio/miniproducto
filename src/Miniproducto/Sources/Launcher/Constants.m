#import "Common/Constants.h"
#import "Launcher/Constants.h"

// LauncherViewController's constants

NSString *const kLauncherTitle = @"Menú";
// NSLocalizedString(@"Menú)", nil)

// toolbar delay

const NSTimeInterval kLauncherToolbarDelay = .25;

// Controller URLs

NSString *const kURLLauncher = @"tt://launcher/";

// Controller URL's calls

NSString *const kURLLauncherCall = @"tt://launcher/";

// Launcher item's icons

NSString *const kURLShoppingListIcon = @"bundle://launcher-icon-lists.png";
NSString *const kURLRecipesIcon = @"bundle://launcher-icon-recipes.png";
NSString *const kURLOffersIcon = @"bundle://launcher-icon-offers.png";
NSString *const kURLStoresIcon = @"bundle://launcher-icon-stores.png";
NSString *const kURLBodyMeterIcon = @"bundle://launcher-icon-calculator.png";
NSString *const kURLSomelierIcon = @"bundle://launcher-icon-somelier.png";
NSString *const kURLCompositionIcon = @"bundle://launcher-icon-composition.png";
NSString *const kURLPhonesIcon = @"bundle://launcher-icon-phones.png";

// Module model's constants

NSString *const kModuleNameKey = @"name";
NSString *const kModulePictureKey = @"picture";

// Banner model's constants

NSString *const kBannerIdKey = @"id";
NSString *const kBannerCodeKey = @"code";
NSString *const kBannerNameKey = @"name";
NSString *const kBannerPictureURLKey = @"picture";

// BannerCollection model's constants

NSString *const kBannerCollectionBannersKey = @"banners";

// Endpoints

NSString *const kBannerListEndpoint = ENDPOINT(@"/carousel.json");