#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import <Three20/Three20.h>

#import "Common/Views/NonEditableLauncherView.h"
#import "Common/Controllers/ConnectedCarouselViewController.h"

@interface LauncherViewController: TTViewController <TTLauncherViewDelegate,
        TTModelDelegate>
{
    NonEditableLauncherView *_launcherView;
    ConnectedCarouselViewController *_carousel;
}
@end
