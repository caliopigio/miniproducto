#import "Common/Constants.h"
#import "Common/Additions/TTStyleSheet+Additions.h"
#import "Launcher/Constants.h"
#import "Launcher/Models.h"
#import "Stores/Models.h"
#import "Stores/Constants.h"
#import "Recipes/Constants.h"
#import "Recipes/Models.h"
#import "Contact/Constants.h"
#import "ListLauncherDataSource.h"
#import "ListLauncherController.h"

static CGFloat const kCarouselHeight = 160.;

@interface ListLauncherController (Private)

- (void)viewTapped:(id)sender;
@end

@implementation ListLauncherController

#pragma mark -
#pragma mark UITableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self != nil) {
        [self setTitle:@"El Buen Recado"];
        [[self navigationItem] setBackBarButtonItem:[[UIBarButtonItem alloc]
                initWithTitle:@"Inicio" style:UIBarButtonItemStyleBordered
                    target:nil action:nil]];
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    CGRect frame = [[self view] frame];
    
    // Carousel
    frame.size.height = kCarouselHeight;
    _carousel = [[ConnectedCarouselViewController alloc] initWithDelegate:self];
    
    [_carousel setCarouselHeight:kCarouselHeight];
    [[_carousel view] setBackgroundColor:[UIColor
            colorWithRed:155./255. green:132./255. blue:92./255. alpha:1.]];
    [[self tableView] setTableHeaderView:[_carousel view]];
    BannerCollection *banners = [[BannerCollection alloc] init];
    
    [[banners delegates] addObject:self];
    [banners load:TTURLRequestCachePolicyEtag more:YES];
    
    RegionCollection *regions = [[RegionCollection alloc] init];
    
    [[regions delegates] addObject:self];
    [regions load:TTURLRequestCachePolicyEtag more:YES];
}

#pragma mark -
#pragma mark TTTableViewController

- (void)createModel
{
    TTTableImageItem *carta = [TTTableImageItem itemWithText:@"Carta"
            imageURL:@"bundle://catalogue_icon.png"
                URL:kURLRecipeCategoriesCall];
    TTTableImageItem *tiendas = [TTTableImageItem itemWithText:@"Tiendas"
            imageURL:@"bundle://stores_icon.png" URL:nil];
    TTTableImageItem *nosotros = [TTTableImageItem itemWithText:@"Nosotros"
            imageURL:@"bundle://contact_icon.png"
                URL:kURLContactDetailControllerCall];
    NSMutableArray *items = [NSMutableArray arrayWithObjects:
            carta, tiendas, nosotros, nil];
    [self setDataSource:[ListLauncherDataSource dataSourceWithItems:items]];
}

#pragma mark -
#pragma mark ListLauncherController (Private)

- (void)viewTapped:(UIGestureRecognizer *)sender
{
    Banner *banner = (Banner *)[(BannerImage *)[sender view] banner];
    NSString *bannerId = [[banner bannerId] stringValue];
    
    [[TTNavigator navigator] openURLAction:[[TTURLAction
            actionWithURLPath:URL(kURLItemDetailCall, bannerId,
                @"El-Buen-Recado")] applyAnimated:YES]];
}

#pragma mark -
#pragma mark <TTModelDelegate>

- (void)modelDidFinishLoad:(id<TTModel>)model
{
    if ([model isKindOfClass:[BannerCollection class]]) {
        for (Banner *banner in [(BannerCollection *)model banners]) {
            [_carousel addViewFromBanner:banner];
        }
    } else if ([model isKindOfClass:[RegionCollection class]]) {
        RegionCollection *regions = (RegionCollection *)model;
        NSString *URL = nil;

        if ([[regions regions] count] > 1) {
            URL = kURLRegionListCall;
        } else if ([[regions regions] count] == 1) {
            Region *region = [[regions regions] objectAtIndex:0];
            
            if ([[region count] integerValue] == 1) {
                URL = URL(kURLStoreListCall, [region suggested],
                        [region regionId], [region name]);
            } else {
                URL = URL(kURLSubregionListCall, [region regionId],
                        [region name]);
            }
        }
        TTTableImageItem *tiendas = [TTTableImageItem itemWithText:@"Tiendas"
                imageURL:@"bundle://stores_icon.png" URL:URL];
        
        [[(TTListDataSource *)_dataSource items] replaceObjectAtIndex:1
                withObject:tiendas];
        [_tableView reloadData];
    }
}

#pragma mark -
#pragma mark <ConnectedCarouselDelegate>

- (void)carousel:(ConnectedCarouselViewController *)carousel
    didLoadImage:(BannerImage *)image
{
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc]
            initWithTarget:self action:@selector(viewTapped:)];
    
    [image addGestureRecognizer:gesture];
}
@end
