#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import <Three20/Three20.h>

#import "Common/Constants.h"
#import "Common/Additions/TTStyleSheet+Additions.h"
#import "Common/Views/NonEditableLauncherView.h"
#import "Application/StyleSheet.h"
#import "Launcher/Constants.h"
#import "Launcher/LauncherViewController.h"
#import "Recipes/Constants.h"
#import "Stores/Constants.h"
#import "Launcher/Models.h"

static CGFloat const kCarouselHeight = 140.;

@interface LauncherViewController (Private)

- (void)hideToolbar:(NSNumber *)animated;
@end

@implementation LauncherViewController

#pragma mark -
#pragma mark NSObject

- (void)dealloc
{
    [_launcherView release];
    [super dealloc];
}

#pragma mark -
#pragma mark UIViewController

- (id)initWithNibName:(NSString *)nibName bundle:(NSBundle *)bundle
{
    if ((self = [super initWithNibName:nibName bundle:bundle]) != nil) {
        [self setTitle:NSLocalizedString(kLauncherTitle, nil)];
    }
    return self;
}

- (void)loadView
{
    [super loadView];

    UIView *superView = [self view];
    CGRect frame = [superView frame];
    // Carousel
    frame.size.height = kCarouselHeight;
    _carousel = [[ConnectedCarouselViewController alloc] init];
    
    [_carousel setCarouselHeight:kCarouselHeight];

    UIView *testView1 = [[UIView alloc] initWithFrame:frame];
    
    [testView1 setBackgroundColor:[UIColor redColor]];
    [_carousel addViewToCarousel:testView1];
    UIView *testView2 = [[UIView alloc] initWithFrame:frame];
    
    [testView2 setBackgroundColor:[UIColor brownColor]];
    [_carousel addViewToCarousel:testView2];
    
    frame = [[_carousel view] frame];
    frame.origin.y = 0;
    [[_carousel view] setFrame:frame];
    [superView addSubview:[_carousel view]];
    PromotionCollection *banner = [[PromotionCollection alloc] init];
    
    [[banner delegates] addObject:self];
    [banner load:TTURLRequestCachePolicyEtag more:YES];
    // Launcher
    frame = [superView frame];
    frame.origin.y = kCarouselHeight;
    frame.size.height -= kCarouselHeight;
    _launcherView = [[NonEditableLauncherView alloc] initWithFrame:frame];
    // Configuring the launcher
    [_launcherView setDelegate:self];
    [_launcherView setColumnCount:2];
    [_launcherView setPages:[NSArray arrayWithObjects:
            [NSArray arrayWithObjects:
                [[[TTLauncherItem alloc] initWithTitle:kRegionLauncherTitle
                    image:kURLStoresIcon
                    URL:kURLRegionListCall] autorelease],
                [[[TTLauncherItem alloc] initWithTitle:kRecipeListTitle
                    image:kURLRecipesIcon
                    URL:kURLRecipeCategoriesCall] autorelease],
                nil],
            nil]];
    //styles
    [_launcherView setBackgroundColor:[UIColor clearColor]];
    [superView setAlpha:.0];
    if ([TTStyleSheet 
            hasStyleSheetForSelector:@selector(launcherBackgroundImage)]) {
        UIImageView *backgroundView = 
                (UIImageView *)TTSTYLE(launcherBackgroundImage);

        [superView addSubview:backgroundView];
        [superView sendSubviewToBack:backgroundView];
    }
    [superView addSubview:_launcherView];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.8];
    [superView setAlpha:1.];
    [UIView commitAnimations]; 
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([TTStyleSheet hasStyleSheetForSelector:@selector(navigationBarLogo)]) {
        [[self navigationItem] setTitleView:[[[UIImageView alloc]
                initWithImage:(UIImage *)TTSTYLE(navigationBarLogo)]
                autorelease]];
    }
    if ([self toolbarItems] == nil) {
        [self performSelector:@selector(hideToolbar:)
                withObject:[NSNumber numberWithBool:animated]
                afterDelay:kLauncherToolbarDelay];
    }
}

#pragma mark -
#pragma mark LauncherViewController (Private)

- (void)hideToolbar:(NSNumber *)animated
{
    [[self navigationController] setToolbarHidden:YES
            animated:[animated boolValue]];
}

#pragma mark -
#pragma mark <TTLauncherViewDelegate>

- (void)launcherView:(TTLauncherView *)launcherView
       didSelectItem:(TTLauncherItem *)item
{
    [[TTNavigator navigator] openURLAction:
            [[TTURLAction actionWithURLPath:[item URL]] applyAnimated:YES]];
}

#pragma mark -
#pragma mark <TTModelDelegate>

- (void)modelDidFinishLoad:(id<TTModel>)model
{
    NSString *url;
    for (Promotion *promotion in [(PromotionCollection *)model promotions]) {
        url = [NSString stringWithFormat:@"%@?width=%.0f",
               [[promotion bannerURL] absoluteString],
                    [[self view] frame].size.width * 2];
        
        [_carousel addViewWithURLToCarousel:url];
    }
}
@end
